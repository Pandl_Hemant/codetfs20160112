function LostQuoteEnableRule() {
    return CreateOrderEnableRule();
}

function CreateOrderEnableRule() {
    if(Xrm.Page.data == undefined || Xrm.Page.data == null) {
          return true;
    }

    var quoteid = Xrm.Page.data.entity.getId();

    if(quoteid == undefined || quoteid == null) {
          return true;
    }

    var fetchXml =
            "<fetch mapping='logical' >" +
               "<entity name='quote'>" +
                  "<attribute name='quoteid' />" +
                  "<filter type='and'>" +
                        "<condition attribute='quoteid' operator='eq' uiname='' uitype='quote' value='" + quoteid + "' />" +
                    "</filter>" +
                  "<link-entity name='salesorder' from='quoteid' to='quoteid' alias='ac'>" +
                    "<attribute name='salesorderid' />" +
                    "<filter type='and'>" +
                        "<condition attribute='statecode' operator='eq' value='0' />" +
                    "</filter>" +
                   "</link-entity>" +
                "</entity>" +
            "</fetch>";

    var fetchedContacts = XrmServiceToolkit.Soap.Fetch(fetchXml);
    return fetchedContacts.length == 0;
}

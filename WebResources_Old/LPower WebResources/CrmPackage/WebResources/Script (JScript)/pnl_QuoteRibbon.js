/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
System: Landpower MSCRM 2011
Author: P&L Limted

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

function btnRecalculate() {
    try {
        //plugin triggered on save. Force save even if not dirty (changes came from external source that didnt trigger plugin)

        if (Xrm.Page.getAttribute("pnl_recalculate").getValue() == 0 ) {
            Xrm.Page.getAttribute("pnl_recalculate").setValue(1);

        }
        else {
            Xrm.Page.getAttribute("pnl_recalculate").setValue(0);

        }

        Xrm.Page.data.entity.save();
        
    }
    catch (err) {
        alert(err.message);
    }
}




﻿using System.Linq;

namespace LandPower.BusinessLogic
{
	public class Utility
	{
		public static string GetNumbers(string input)
		{
			return new string(input.Where(char.IsDigit).ToArray());
		}
	}
}

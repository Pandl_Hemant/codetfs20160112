﻿using System;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace LandPower.Data
{
	public interface IUserExtensions
	{
		SystemUser FetchSystemUserById(Guid userId);
	}

	public static class SystemUserExtensions
	{
		public static Func<CrmServiceContext, IUserExtensions> UserFactory = serviceContext => new UserExtensions(serviceContext); // Could be internal with InternalsVisibleTo in tests project.

		public static IUserExtensions User(this CrmServiceContext serviceContext)
		{
			return UserFactory(serviceContext);
		}
	}

	internal class UserExtensions : IUserExtensions
	{
		private readonly CrmServiceContext _serviceContext;

		public UserExtensions(CrmServiceContext serviceContext)
		{
			_serviceContext = serviceContext;
		}

		public SystemUser FetchSystemUserById(Guid userId)
		{
			return (from u in _serviceContext.SystemUserSet
					where u.SystemUserId.Value == userId
					select u).FirstOrDefault();
		}
	}
}

﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("LandPower.Data")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("LandPower.Data")]
[assembly: AssemblyCopyright("Copyright ©  2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("da925cae-0f8c-4008-b880-df01a2773698")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
//[assembly: InternalsVisibleTo("LandPower.Data.Tests, PublicKey=0024000004800000940000000602000000240000525341310004000001000100c95145363102b0" +
//"907ebd906f930683535740a7630e32757f324e46c84b10567fc7c6d1284254e97c2bff2721fe00" +
//"b1322ef2e76ef39c5bcfbfeb56ec6812ffa6f0a9aeaaaea3d2e5243c03914e6370ba29e66ab608" +
//"3e8289403e6be189b93ccbcaefc26f8489e94d8250517a92f8ed3b38ca8bf45ff329a0915a8def" +
//"479cd7ba")]
//[assembly: InternalsVisibleTo("LandPower.BusinessLogic.Tests, PublicKey=0024000004800000940000000602000000240000525341310004000001000100c95145363102b0" +
//"907ebd906f930683535740a7630e32757f324e46c84b10567fc7c6d1284254e97c2bff2721fe00" +
//"b1322ef2e76ef39c5bcfbfeb56ec6812ffa6f0a9aeaaaea3d2e5243c03914e6370ba29e66ab608" +
//"3e8289403e6be189b93ccbcaefc26f8489e94d8250517a92f8ed3b38ca8bf45ff329a0915a8def" +
//"479cd7ba")]
[assembly: InternalsVisibleTo("LandPower.Data.Tests")]
[assembly: InternalsVisibleTo("LandPower.BusinessLogic.Tests")]
[assembly: System.Security.AllowPartiallyTrustedCallers]

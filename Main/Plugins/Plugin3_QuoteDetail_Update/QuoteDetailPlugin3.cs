using System;
using System.Linq;
using System.ServiceModel;
using Microsoft.Xrm.Sdk;
using LandPower;
  

namespace LandPowerPlugins
{
    public class QuoteDetailPlugin3 : LandpowerPluginBase, IPlugin
	{
        public void Execute(IServiceProvider serviceProvider)
        {
            //Extract the tracing service for use in debugging sandboxed plug-ins.
            ITracingService tracingService =
                (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            // Obtain the execution context from the service provider.
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            if (context.Depth > 1)
                return;

            // The InputParameters collection contains all the data passed in the message request.
            if (context.InputParameters.Contains("Target") &&
                context.InputParameters["Target"] is Entity)
            {
                // Obtain the target entity from the input parameters.
                Entity sourceEntity = (Entity)context.InputParameters["Target"];

                // Verify that the target entity represents an account.
                // If not, this plug-in was not registered correctly.
                if (sourceEntity.LogicalName != "quotedetail")
                    return;

                try
                {
                    // Obtain the organization service reference.
                    IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                    IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);

               
                    // next get the PostImage of the quote detail
                    QuoteDetail quoteDetailPostImage = null;
                    if (context.PostEntityImages.Contains("quotedetail") && (context.PostEntityImages["quotedetail"] is Entity))
                    {
                        Entity thisQDEntity = (Entity)context.PostEntityImages["quotedetail"];
                        if (thisQDEntity.LogicalName == "quotedetail")
                        {
                            quoteDetailPostImage = thisQDEntity.ToEntity<QuoteDetail>();
                        }
                    }

                    // use the source entity for updating
                    QuoteDetail quoteDetailUpdate = sourceEntity.ToEntity<QuoteDetail>();

                    // the list price is reused multiple times so get it once 
                    decimal listPrice = GetListPrice(quoteDetailPostImage);

                    // Use reference to invoke pre-built types service 
                    var ctx = new LandPower.crmServiceContext(service);

                    // calculate new values on this quote detail record
                    CalculateDealerNetPrice(ctx, quoteDetailPostImage, listPrice, quoteDetailUpdate);
                    CalculateLocallySourcedItemStandardDiscount(quoteDetailUpdate, listPrice, quoteDetailPostImage);

                    // update the quote detail on the server
                    service.Update(quoteDetailUpdate);

                }
                catch (FaultException<OrganizationServiceFault> ex)
                {
                    throw new InvalidPluginExecutionException("An error occurred in the QuoteProductUpdatePlugin plug-in.", ex);
                }

                catch (Exception ex)
                {
                    tracingService.Trace("QuoteProductUpdatePlugin: {0}", ex.ToString());
                    throw;
                }
            }
        }

        private static void CalculateDealerNetPrice(crmServiceContext ctx, QuoteDetail quoteDetailPostImage, decimal listPrice, QuoteDetail quoteDetailUpdate)
        {
            if ((quoteDetailPostImage.pnl_islocallysourced == null || quoteDetailPostImage.pnl_islocallysourced.Value == YES_OR_NO_FALSE) 
                  && (!quoteDetailPostImage.pnl_IsTradeIn.HasValue || !quoteDetailPostImage.pnl_IsTradeIn.Value))
            {
            
                decimal? standardDiscount    = null;
                decimal? indentDiscount      = null;
                decimal? monthlyProgPercent  = null;
                decimal? monthlyProgDollar   = null;

                if (quoteDetailPostImage.QuoteId == null)
                    throw new InvalidPluginExecutionException("This Quote Product has no Quote specified.");

                Guid thisQuoteId = quoteDetailPostImage.QuoteId.Id;
#if true
                Quote thisQuote= (from q in ctx.QuoteSet 
                                       where q.Id == thisQuoteId
                                       select q).FirstOrDefault();

                if (thisQuote == null || thisQuote.OwningUser == null)
                    throw new InvalidPluginExecutionException("Unable to locate Owner for related Quote.");


                SystemUser thisUser = (from u in ctx.SystemUserSet
                                    where u.Id == thisQuote.OwningUser.Id
                                    select u).FirstOrDefault();

                if (thisUser == null || thisUser.BusinessUnitId == null)
                    throw new InvalidPluginExecutionException("Unable to locate Business Unit for the Owner of the related Quote.");


                BusinessUnit ownersBu = (from bu in ctx.BusinessUnitSet
                                       where bu.Id == thisUser.BusinessUnitId.Id
                                       select bu).FirstOrDefault();
#else
                /*
                 * This should work(?) however error is:-
                 *      AttributeFrom and AttributeTo must be either both specified or both ommited. 
                 *      You can not pass only one or the other. AttributeFrom: , AttributeTo: owninguser
                 * */

                BusinessUnit ownersBu = (from bu in ctx.BusinessUnitSet
                                         join su in ctx.SystemUserSet on bu.Id equals su.BusinessUnitId.Id
                                         join qu in ctx.QuoteSet on su.Id equals qu.OwningUser.Id
                                         where qu.Id == thisQuoteId
                                         select bu).FirstOrDefault();
#endif

                               
                if(ownersBu == null || ownersBu.pnl_BusinessUnitCountry == null)
                    throw new InvalidPluginExecutionException("The Quote owner must be assigned to a Business Unit that has a Country specified.");


                // pnl_mod_sup_stock_code = model code 
                // pnl_specs_sales_stock_code = , stock code 
                // pnl_InStkSerial_no = serial no
                // date range applies
                // quoteProduct's Quote's Owner's business Unit's BU Country matches the discount record's COuntry.


                DateTime todaysDateOnly = DateTime.Now.Date;

                // get both sets of discounts in the one round-trip
                var discountInSet = from t in ctx.pnl_discountSet
                                 where (
                                    (t.pnl_modelcode == null || t.pnl_modelcode.Equals(quoteDetailPostImage.pnl_mod_sup_stock_code) )
                                    && 
                                    t.pnl_StockCode.Equals(quoteDetailPostImage.pnl_specs_sales_stock_code))
                                 || t.pnl_SerialNo.Equals(quoteDetailPostImage.pnl_InStkserial_no)
                                    //where ((t.pnl_DateFrom.HasValue.Equals(false) || t.pnl_DateFrom.Value <= todaysDateOnly)
                                    //   && (t.pnl_DateTo.HasValue.Equals(false) || t.pnl_DateTo.Value >= todaysDateOnly))  // SEE TODO BELOW
                                 where t.pnl_Country.Value == ownersBu.pnl_BusinessUnitCountry.Value
                                 orderby t.pnl_SerialNo descending // should sort serial nos first
                                 select t;

                bool standardDiscountApplied = false;
                bool indentDiscountApplied = false;
                bool programmeDiscountApplied = false;

                // first process serialno matches, and then the rest if still not defined - ie the respective decimals are still null
                foreach (var disc in discountInSet)
                {
                    bool codeMatchFound = false;

                    if (disc.pnl_SerialNo != null && !string.IsNullOrWhiteSpace(disc.pnl_SerialNo))
                    {
                        // a serial number was specified on the discount line
                        codeMatchFound = disc.pnl_SerialNo.Equals(quoteDetailPostImage.pnl_InStkserial_no);
                    }

                    if(disc.pnl_StockCode != null && !string.IsNullOrWhiteSpace(disc.pnl_StockCode))
                    {
                        // a stockcode was specified on the discount line
                        codeMatchFound = disc.pnl_StockCode.Equals(quoteDetailPostImage.pnl_specs_sales_stock_code);

                        if(codeMatchFound && disc.pnl_modelcode != null && !string.IsNullOrWhiteSpace(disc.pnl_modelcode))
                        {
                            // a model code was also specifed on the discount line so it must also match when the stock code matches
                            codeMatchFound = disc.pnl_modelcode.Equals(quoteDetailPostImage.pnl_mod_sup_stock_code);
                        }
                    }

                    if(!codeMatchFound)
                        continue;

                    // ignore discounts that do not include today in their date range (if specified)
                    // TODO: remove this and only return matching records in above linq
                    // was returning this exception:- "Invalid 'where' condition. An entity member is invoking an invalid property or method."
                    if ((disc.pnl_DateFrom.HasValue && disc.pnl_DateFrom.Value > todaysDateOnly) || (disc.pnl_DateTo.HasValue && disc.pnl_DateTo.Value < todaysDateOnly))
                        continue;




                    switch((LandpowerPluginBase.DiscountType)disc.pnl_DiscountType.Value)
                    {
                        case DiscountType.STANDARD:
                            if (!standardDiscountApplied)
                            {
                                standardDiscount = ValueAsDecimal(disc.pnl_DiscountPercentage);
                                standardDiscountApplied = true;
                            }
                            break;
                        case DiscountType.INDENT:
                            if (!indentDiscountApplied)
                            {
                                indentDiscount = ValueAsDecimal(disc.pnl_DiscountPercentage);
                                indentDiscountApplied = true;
                            }
                            break;
                        case DiscountType.PROGRAMME:
                            if (!programmeDiscountApplied)
                            {
                                // only use one of these 2 - use percent if defined else the dollars if defined, 
                                // else use nothing (but still note this record exists and defines nothing be used).
                                if (disc.pnl_DiscountPercentage.HasValue && disc.pnl_DiscountPercentage != DECIMAL0)
                                    monthlyProgPercent = ValueAsDecimal(disc.pnl_DiscountPercentage);
                                else if (quoteDetailPostImage.pnl_specType != null 
                                    && (SpecificationType)quoteDetailPostImage.pnl_specType.Value == SpecificationType.PRODUCT  
                                    &&  disc.pnl_DiscountDollar != null 
                                    && disc.pnl_DiscountDollar.Value != DECIMAL0)
                                    monthlyProgDollar = ValueAsDecimal(disc.pnl_DiscountDollar);
                                programmeDiscountApplied = true;
                            }
                            break;
                    }

                    // only apply on serial number discount - these should have been encountered first if they exist
                    if (disc.pnl_SerialNo != null)
                        break;
                }

                decimal standardDiscountDollars = standardDiscount.HasValue ? listPrice * standardDiscount.Value / DECIMAL100 : DECIMAL0;
                decimal indentDiscountDollars = indentDiscount.HasValue ? listPrice * indentDiscount.Value / DECIMAL100 : DECIMAL0;
                decimal monthlyProgPercentDollars = monthlyProgPercent.HasValue ? listPrice * monthlyProgPercent.Value / DECIMAL100 : DECIMAL0;

                quoteDetailUpdate.pnl_MachineCostsStandardDiscount = standardDiscount;
                quoteDetailUpdate.pnl_MachineCostsIndentDiscount = indentDiscount;
                quoteDetailUpdate.pnl_machinecostsmonthlyprogpercent = monthlyProgPercent;
                quoteDetailUpdate.pnl_MachineCostsMonthlyProgDollar = monthlyProgDollar.HasValue ? new Money(monthlyProgDollar.Value) : null;


                decimal machineCostsDiscountDollars = 
                    standardDiscountDollars
                    + indentDiscountDollars
                    + monthlyProgPercentDollars
                    + (monthlyProgDollar.HasValue?monthlyProgDollar.Value:DECIMAL0);

                quoteDetailUpdate.pnl_MachineCostsDealerNetPrice = new Money(listPrice - machineCostsDiscountDollars);

                quoteDetailUpdate.pnl_MachineCostsDiscountPercent = listPrice == DECIMAL0 ? DECIMAL0 :
                    (machineCostsDiscountDollars / listPrice) * 100;
            }
        }

        private static void CalculateLocallySourcedItemStandardDiscount(QuoteDetail quoteDetailUpdate, decimal listPrice, QuoteDetail quoteDetailPostImage)
        {
            decimal locallySourcedDNP = quoteDetailPostImage.pnl_LocallySourcedDNP != null ? quoteDetailPostImage.pnl_LocallySourcedDNP.Value : 0m;

            if (listPrice == 0 || locallySourcedDNP == 0)
                quoteDetailUpdate.pnl_LocallySourcedStandardDiscount = null;
            else
                quoteDetailUpdate.pnl_LocallySourcedStandardDiscount = 100 * (listPrice - locallySourcedDNP) / listPrice;
        }

        private static decimal GetListPrice(QuoteDetail quoteDetailPostImage)
        {
            decimal pricePerUnit = quoteDetailPostImage.PricePerUnit != null ? quoteDetailPostImage.PricePerUnit.Value : 0m;
            decimal quantity = quoteDetailPostImage.Quantity.HasValue ? quoteDetailPostImage.Quantity.Value : 0m;
            decimal listPrice = decimal.Round(pricePerUnit * quantity, 2);
            return listPrice;
        }
    }
}





using CrmEarlyBound;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using System;
using System.Linq;
using System.ServiceModel;


namespace LandPower
{
    public class AppraisalItem_PostUpdate : IPlugin
    {
        private const string  statusCodeFieldName = "statuscode";
        private const string pnl_appraisalRecalcedOnFieldName = "pnl_appraisalrecalcedon";
        private const string pnl_syncUpdatedOnFieldName = "pnl_syncupdatedon";
        private const string pnl_appraisalIdFieldName = "pnl_appraisalid";

        #region Private Vars

        private IOrganizationService _service;
        private CrmServiceContext _ctx;
        private Entity _updateEntity;

        #endregion

        #region Public Methods

        public void Execute(IServiceProvider serviceProvider)
        {
            //Extract the tracing service for use in debugging sandboxed plug-ins.
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            // Obtain the execution context from the service provider.
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            // don't want to redo create logic when resyncing online
            if (context.IsOfflinePlayback)
                return;

            // The InputParameters collection contains all the data passed in the message request.
            if (!context.InputParameters.Contains("Target") || !(context.InputParameters["Target"] is Entity))
                return;

            if (context.Depth > 3) return;

            // Obtain the target entity from the input parameters.
            _updateEntity = (Entity)context.InputParameters["Target"];

             // Verify that the target entity represents an appraisal.
            // If not, this plug-in was not registered correctly.
            if (_updateEntity.LogicalName != pnl_appraisalitem.EntityLogicalName)
                return;

            try
            {
                // Obtain the organization service reference.
                IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                _service = serviceFactory.CreateOrganizationService(context.UserId);

                // Use reference to invoke pre-built types service 
                _ctx = new CrmServiceContext(_service);

                Entity appraisalItemPreImageEntity = null;
                if (context.PreEntityImages.Contains("PreImage") && (context.PreEntityImages["PreImage"] is Entity))
                {
                    appraisalItemPreImageEntity = (Entity)context.PreEntityImages["PreImage"];
                }
                if (appraisalItemPreImageEntity == null || appraisalItemPreImageEntity.LogicalName != pnl_appraisalitem.EntityLogicalName)
                {
                    throw new InvalidPluginExecutionException("Incorrect Plugin registration: a 'pnl_appraisalitem' Pre-image has not been correctly configured for this plugin.");
                }

                Guid appraisalId;
                if (_updateEntity.Attributes.ContainsKey(pnl_appraisalIdFieldName) && _updateEntity[pnl_appraisalIdFieldName] != null)
                    appraisalId = ((EntityReference)_updateEntity[pnl_appraisalIdFieldName]).Id;
                else if (appraisalItemPreImageEntity.Attributes.ContainsKey(pnl_appraisalIdFieldName) && appraisalItemPreImageEntity[pnl_appraisalIdFieldName] != null)
                    appraisalId = ((EntityReference)appraisalItemPreImageEntity[pnl_appraisalIdFieldName]).Id;
                else
                {
                    throw new InvalidPluginExecutionException("Unable to locate AppraisalId for Appraisal Item");
                }

                pnl_appraisal appraisal = _ctx.pnl_appraisalSet.FirstOrDefault(x => x.Id.Equals(appraisalId));
                if(appraisal == null)
                {
                    throw new InvalidPluginExecutionException("Unable to locate Appraisal for Appraisal Item");
                }

                // raise exception if this user does nto have the rights to update the Appraisal
                CheckCanApplyChanges(context.InitiatingUserId, appraisal, appraisalItemPreImageEntity);

                // If this Appraisal Item has been updated as part of an iTrade sync or the Appraisal Update 
                // plugin (typically hollistically resetting Names or Values) then dont do anything more - just let the udpate complete
                if (_updateEntity.Attributes.Contains(pnl_appraisalRecalcedOnFieldName)) return;
                if (_updateEntity.Attributes.Contains(pnl_syncUpdatedOnFieldName)) return;

                // update the appraisal field that triggers a reprocess of the apppraisal and it's child items - including this one.
                var appraisalUpdate = new pnl_appraisal() { Id = appraisalId, pnl_itemupdatedon = DateTime.Now.ToLocalTime() };
                _service.Update(appraisalUpdate);                

            }
            #region catch - Exception handling
            catch (FaultException<OrganizationServiceFault> ex)
            {
               throw new InvalidPluginExecutionException(string.Format("An error occurred in the Appraisal Update plug-in:-{0}{1}", Environment.NewLine, ex.Message) );
            }
            #endregion
        }

        #endregion

        #region Private Methods
        // Start of Change 
        // Change Request 18023 - Changed By - RG - Change Date - 15.12.2015
        private void CheckCanApplyChanges(Guid userId, pnl_appraisal thisAppraisal, Entity thisappItem)
        {
            bool canUpdate = false;
            var user = new SystemUser { Id = userId };

            if (thisAppraisal.statuscode.Value == (int)pnl_appraisal_statuscode.ApprovedApprasial
             || thisAppraisal.statuscode.Value == (int)pnl_appraisal_statuscode.ApprovedAppraisal)
            {
                canUpdate = IsInRole(userId, "System Administrator") || IsInRole(userId, "System Administrator � Sales");
                if (IsInRole(userId, "Appraisal User") && (thisAppraisal.Contains("pnl_descriptionnotes") || thisAppraisal.Contains("pnl_itemupdatedon"))
                       || thisAppraisal.Contains("pnl_defaultappraisalphotoid"))
                {
                    canUpdate = true;
                }

                //var apItem = (from ap in _ctx.pnl_appraisalitemSet
                //              where ap.Id.Equals(thisappItem.Id)
                //              select ap).FirstOrDefault();
                //pnl_appraisalitem appItem = apItem.ToEntity<pnl_appraisalitem>();
                //if (appItem.Contains("pnl_appraisalitemgroupid"))
                //{
                //    var apgroupItem = (from gi in _ctx.pnl_appraisalitemgroupSet
                //                       where gi.Id.Equals(appItem.pnl_AppraisalItemGroupId.Id)
                //                       select gi).FirstOrDefault();
                //    pnl_appraisalitemgroup groupItem = apgroupItem.ToEntity<pnl_appraisalitemgroup>();
                //    if (groupItem.Contains("pnl_groupnameid") && groupItem.pnl_GroupNameId.Name == "Specification")
                //    {
                //        canUpdate = true;
                //    }
                    
                //}

            }

            else if (thisAppraisal.statuscode.Value == (int)pnl_appraisal_statuscode.Inspection_1 ||
                thisAppraisal.statuscode.Value == (int)pnl_appraisal_statuscode.Inspection_2)
            {
                canUpdate = true;
            }
            else
            {
                //canUpdate = IsTeamMember(userId, "Appraisal Approvers");
                canUpdate = IsTeamMember(userId, "Appraisal Approvers") || IsInRole(userId, "Appraisal Administrator");
            }

            if (!canUpdate)
            {
                const string message =
                    "Permission denied. The Appraisal can be updated if it is in inspection, or you are an approver and it is waiting for approval, " +
                        "or you are a System Administrator and it has been Approved.";
                throw new InvalidPluginExecutionException(message);
            }
        }
        //End of Changes
        private bool IsTeamMember(Guid userId, string teamName)
        {
            var teamMember = (from tm in _ctx.TeamMembershipSet
                              join t in _ctx.TeamSet on tm.TeamId.GetValueOrDefault() equals t.Id
                              where tm.SystemUserId.GetValueOrDefault() == userId && t.Name == teamName
                              select tm).FirstOrDefault();

            return teamMember != null;
        }

        private bool IsInRole(Guid userId, string roleName)
        {
            var userRole = (from s in _ctx.SystemUserRolesSet
                            join r in _ctx.RoleSet on s.RoleId.Value equals r.RoleId.Value
                            where s.SystemUserId.Value == userId
                                && r.Name == roleName
                            select s).FirstOrDefault();

            return userRole != null;
        }

        #endregion

    }
}

﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LandPowerPlugins.Extensions;

namespace LandPowerPlugins
{
    class AppraisalItemPhotoUpdate
    {
        public AppraisalItemPhotoUpdate(IOrganizationService service, IPluginExecutionContext context, CrmServiceContext crmContext, Entity preImageEntity, Entity postImageEntity, Entity updateEntity)
        {
            _service = service;
            context = _context;
            _crmContext = crmContext;
            _preImageEntity = preImageEntity;
            _postImageEntity = postImageEntity;
            _inputEntity = updateEntity;
        }

        public void ExecuteAppraisalItemPhotoUpdate()
        {
            // Use preimage on update and also the inputParams so that appraisal cannot be changed as a workaround.
            var appraisalPhoto = _preImageEntity == null ? _postImageEntity == null ? null : _postImageEntity.ToEntity<pnl_appraisalphoto>() : _preImageEntity.ToEntity<pnl_appraisalphoto>();
            if (null == appraisalPhoto) return;

            if (appraisalPhoto.pnl_AppraisalId == null) return;

            CheckCanUpdateAppraisal(appraisalPhoto.pnl_AppraisalId.Id);

            if (_context.MessageName.ToLower() != "update") return;

            if (null == _inputEntity || !_inputEntity.Contains("pnl_appraisalid")) return;

            var inputAppraisalId = ((EntityReference)_inputEntity["pnl_appraisalid"]).Id;
            if (inputAppraisalId == appraisalPhoto.pnl_AppraisalId.Id) return;
            CheckCanUpdateAppraisal(inputAppraisalId);
        }

        internal void CheckCanUpdateAppraisal(Guid appraisalId)
        {
            var appraisal = _crmContext.Appraisal().FetchAppraisalById(appraisalId);
            var canUpdate = appraisal.CanApplyChanges(_context.InitiatingUserId, _crmContext);

            if (canUpdate) return;

            throw new InvalidPluginExecutionException("Permission denied. The Appraisal can be updated if it is in inspection or you are an approver and it is waiting for approval.");
        }

        #region Private Vars

        IOrganizationService _service;
        CrmServiceContext _crmContext;
        IPluginExecutionContext _context;
        private Entity _preImageEntity;
        private Entity _postImageEntity;
        private Entity _inputEntity;

        #endregion
    }
}

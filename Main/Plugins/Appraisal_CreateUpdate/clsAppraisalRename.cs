﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LandPowerPlugins
{
    class AppraisalRename
    {
        #region Consts

        private const string DefaultValue = "<Not Specified>";


        
        struct AppraisalNames
        {
            //have to have a "dummy" parameter as there aren't default constructors in this version of C#
            public AppraisalNames(int i)
            {
                CustomerName = DefaultValue;
                MachineType = DefaultValue;
            }

            public string CustomerName;
            public string MachineType;
        }

        #endregion

        #region Properties

        public CrmServiceContext Context { get; set; }

        #endregion

        public string UpdateAppraisalName(pnl_appraisal appraisal)
        {
            const string format = "{0} : {1} : {2}";

            var names = GetNameParametersFromAppraisalItems(appraisal.Id);
            var customer = names.CustomerName;
            var machineType = names.MachineType;
            var appraisalDate = appraisal.pnl_appraisaldate.GetValueOrDefault();
            var appraisalDateValue = appraisalDate == DateTime.MinValue ? DefaultValue : appraisalDate.ToLocalTime().ToString("dd/M/yy");

            return string.Format(format, customer, appraisalDateValue, machineType);
        }

        //take each appraisal item, see if it's name requires changing and change if so
        //format is AppraisalName : ItemName
        public void UpdateChildItems(Guid appraisalId, String appraisalName, IOrganizationService serviceInstance)
        {
            var appraisalItems = from ai in Context.pnl_appraisalitemSet
                                 join n in Context.pnl_appraisalitemnameSet on ai.pnl_AppraisalItemNameId.Id equals n.Id
                                 where ai.pnl_AppraisalId.Id == appraisalId
                                 select new { Id = ai.Id, ItemName = n.pnl_name, CurrentName = ai.pnl_name };

            var appraisalItemGroupItems = from aig in Context.pnl_appraisalitemgroupSet
                                          join n in Context.pnl_appraisalitemgroupSet on aig.pnl_GroupNameId.Id equals n.Id
                                          where aig.pnl_AppraisalId.Id == appraisalId
                                          select new { Id = aig.Id, ItemName = n.pnl_name, CurrentName = aig.pnl_name };

            var appraisalPhotoItems = from aip in Context.pnl_appraisalphotoSet
                                      select new { Id = aip.Id, CurrentName = aip.pnl_name, PhotoNumber = aip.pnl_DisplayOrder };

            //update appraisal items
            foreach (var appraisalItem in appraisalItems)
            {
                var newName = String.Format("{0} : {1}", appraisalName, appraisalItem.ItemName);
                if (newName == appraisalItem.CurrentName) continue;
                var item = new pnl_appraisalitem();
                item.Id = appraisalItem.Id;
                item.pnl_name = newName;
                serviceInstance.Update(item);
            }

            //update appraisal item groups
            foreach (var appraisalItemGroup in appraisalItemGroupItems)
            {
                var newName = String.Format("{0} : {1}", appraisalName, appraisalItemGroup.ItemName);
                if (newName == appraisalItemGroup.CurrentName) continue;

                var item = new pnl_appraisalitemgroup();
                item.Id = appraisalItemGroup.Id;
                item.pnl_name = newName;
                serviceInstance.Update(item);
            }

            //update appraisal photos
            foreach (var appraisalItemPhoto in appraisalPhotoItems)
            {
                var newName = String.Format("{0} : {1}", appraisalName, appraisalItemPhoto.PhotoNumber);
                if (newName == appraisalItemPhoto.CurrentName) continue;

                var item = new pnl_appraisalphoto();
                item.Id = appraisalItemPhoto.Id;
                item.pnl_name = newName;
                serviceInstance.Update(item);
            }
        }

        #region Private Methods

        private AppraisalNames GetNameParametersFromAppraisalItems(Guid appraisalId)
        {
            var names = new AppraisalNames(1); //just calling the parameter as we can't have default constructors in this verson of C#

            var item = (from a in Context.pnl_appraisalitemSet
                        //join n in Context.pnl_appraisalitemnameSet on a.pnl_AppraisalItemNameId.Id equals n.Id
                        where a.pnl_AppraisalId.Id == appraisalId
                              && a.statecode == pnl_appraisalitemState.Active
                              && (a.pnl_CalcCode == "CUSTOMER_NAME" || a.pnl_CalcCode == "MACHINE_TYPE") // *** CHANGE FROM NAMES TO CODES.
                        select a);
            foreach (var i in item)
            {
                var value = i.pnl_Value ?? DefaultValue;

                if (i.pnl_CalcCode.Contains("CUSTOMER_NAME"))
                    names.CustomerName = value;
                else if (i.pnl_CalcCode.Contains("MACHINE_TYPE"))
                    names.MachineType = value;
            }

            return names;
        }

        #endregion
    }
}

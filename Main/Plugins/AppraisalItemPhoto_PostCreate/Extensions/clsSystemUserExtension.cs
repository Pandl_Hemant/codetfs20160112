﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LandPowerPlugins.Extensions
{
    public static class SystemUserExtension
    {
        public static bool IsAway(this SystemUser user)
        {
            return user.pnl_isaway.GetValueOrDefault() && !user.IsDisabled.GetValueOrDefault();
        }

        public static bool IsTeamMember(this SystemUser user, string teamName, CrmServiceContext serviceContext)
        {
            var teamMember = (from tm in serviceContext.TeamMembershipSet
                              join t in serviceContext.TeamSet on tm.TeamId.GetValueOrDefault() equals t.Id
                              where tm.SystemUserId.GetValueOrDefault() == user.Id && t.Name == teamName
                              select tm).FirstOrDefault();

            return teamMember != null;
        }

        public static bool IsInRole(this SystemUser user, string roleName, CrmServiceContext serviceContext)
        {
            var userRole = (from s in serviceContext.SystemUserRolesSet
                            join r in serviceContext.RoleSet on s.RoleId.Value equals r.RoleId.Value
                            where s.SystemUserId.Value == user.Id
                                && r.Name == roleName
                            select s).FirstOrDefault();

            return userRole != null;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LandPowerPlugins;
using Microsoft.Xrm.Sdk;

namespace LandPowerPlugins.Extensions
{
    public static class AppraisalExtension
    {


        public static bool CanApplyChanges(this pnl_appraisal appraisal, Guid userId, CrmServiceContext serviceContext)
        {
            var user = new SystemUser { Id = userId };

            if (appraisal.statuscode.Value == (int)pnl_appraisal_statuscode.ApprovedApprasial_125760001 || appraisal.statuscode.Value ==
                    (int)pnl_appraisal_statuscode.ApprovedApprasial_125760003)
            {
                return user.IsInRole("System Administrator", serviceContext);
            }

            if (appraisal.statuscode.Value == (int)pnl_appraisal_statuscode.Inspection_1 ||
                appraisal.statuscode.Value == (int)pnl_appraisal_statuscode.Inspection_2)
            {
                return true;
            }

            return user.IsTeamMember("Appraisal Approvers", serviceContext);
        }

    }
}

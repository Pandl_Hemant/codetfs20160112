﻿using System;
using System.Linq;
using System.ServiceModel;
using Microsoft.Xrm.Sdk;
using LandPower;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using System.Data.SqlTypes;
using System.Text;
using System.Collections.Generic;

namespace LandPowerPlugins
{
    public class PrimaryContact : IPlugin
    {
        crmServiceContext _ctx;

        public void Execute(IServiceProvider serviceProvider)
        {
            if (serviceProvider == null)
            {
                throw new ArgumentNullException("serviceProvider");
            }

            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            if (context.Depth > 1)
                return;

            if (context.InputParameters.Contains("Target"))
            {
                try
                {
                    IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                    IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);
                    _ctx = new crmServiceContext(service);

                    if (context.InputParameters["Target"] is Entity)
                    {
                        Entity target = (Entity)context.InputParameters["Target"];
                        if (target.LogicalName == Contact.EntityLogicalName && context.PostEntityImages.Contains("ContactPostImage"))
                        {
                            Contact contact = (Contact)context.PostEntityImages["ContactPostImage"].ToEntity<Contact>();

                            //Contact is selected as Primary Contact
                            if (contact.pnl_PrimaryContact != null && contact.pnl_PrimaryContact.HasValue && contact.pnl_PrimaryContact.Value &&
                                contact.ParentCustomerId != null && contact.ParentCustomerId.LogicalName == "account")
                            {
                                RemoveCurrentPrimaryContactFlag(contact.ParentCustomerId.Id, contact.Id);
                                SetAccountsPrimaryContact(contact.ParentCustomerId.Id, contact.Id);
                                if (contact.pnl_ProntoContact != null && contact.pnl_ProntoContact.HasValue && contact.pnl_ProntoContact.Value)
                                    contact.pnl_DisplayOrder = 2;
                                else
                                    contact.pnl_DisplayOrder = 1;
                            }
                            //Contact is selected as Pronto Contact
                            else if (contact.pnl_ProntoContact != null && contact.pnl_ProntoContact.HasValue && contact.pnl_ProntoContact.Value &&
                                contact.ParentCustomerId != null && contact.ParentCustomerId.LogicalName == "account")
                            {
                                    contact.pnl_DisplayOrder = 2;
                            }
                            //Contact is not a Primary or Pronto Contact
                            else
                            {
                                contact.pnl_DisplayOrder = 3;
                            }

                            if (contact.pnl_PrimaryContact == null || (contact.pnl_PrimaryContact.HasValue && !contact.pnl_PrimaryContact.Value &&
                               contact.ParentCustomerId != null && contact.ParentCustomerId.LogicalName == "account"))
                            {
                                RemoveAccountsPrimaryContact(contact.ParentCustomerId.Id, contact.Id);
                            }

                            service.Update(contact);
                        }
                    }
                }               
                catch (Exception ex)
                {
                     //throw new InvalidPluginExecutionException(string.Format("PrimaryContact plugin exception: {0}, {1}, {2}", ex.Message, ex.InnerException, ex.StackTrace));
                    throw new InvalidPluginExecutionException("Only the owner of the customer can set the primary contact.");
                }
                finally
                {
                    _ctx.Dispose();
                }
            }
        }

        private void RemoveCurrentPrimaryContactFlag(Guid accountid, Guid contactid)
        {
            var contactList = from c in _ctx.ContactSet
                              where c.ParentCustomerId.Id.Equals(accountid) 
                              && c.pnl_PrimaryContact.Value.Equals(true)
                              && c.Id != contactid
                              select c;

            bool changes = false;

            foreach (Contact contact in contactList)
            {
                contact.pnl_PrimaryContact = false;

                if (contact.pnl_ProntoContact != null && contact.pnl_ProntoContact.HasValue && contact.pnl_ProntoContact.Value)
                    contact.pnl_DisplayOrder = 2;
                else
                    contact.pnl_DisplayOrder = 3;

                _ctx.UpdateObject(contact);
                changes = true;
            }

            if (changes)
            {
                _ctx.SaveChanges();
            }
        }

        private void SetAccountsPrimaryContact(Guid accountid, Guid contactid)
        {
            Account account = (from a in _ctx.AccountSet where a.Id.Equals(accountid) select a).FirstOrDefault();
            Entity updateEntity = new Entity(Account.EntityLogicalName) { Id = accountid };

            if (account != null)
            {
                if (account.PrimaryContactId == null || account.PrimaryContactId.Id != contactid)
                {
                    updateEntity.Attributes.Add("primarycontactid", new EntityReference(Contact.EntityLogicalName, contactid));
                    _ctx.ClearChanges();
                    _ctx.Attach(updateEntity);
                    _ctx.UpdateObject(updateEntity);
                    _ctx.SaveChanges();
                }
            }
        }

        private void RemoveAccountsPrimaryContact(Guid accountid, Guid contactid)
        {
            Account account = (from a in _ctx.AccountSet where a.Id.Equals(accountid) && a.PrimaryContactId.Id.Equals(contactid) select a).FirstOrDefault();
            if (account != null)
            {
                Entity updateEntity = new Entity(Account.EntityLogicalName) { Id = accountid };
                updateEntity.Attributes.Add("primarycontactid", null);
                _ctx.ClearChanges();
                _ctx.Attach(updateEntity);
                _ctx.UpdateObject(updateEntity);
                _ctx.SaveChanges();
            }
        }
        
    }
}
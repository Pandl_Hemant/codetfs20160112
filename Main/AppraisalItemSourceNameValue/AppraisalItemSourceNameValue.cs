﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
namespace AppraisalItemSourceNameValue
{
    class AppraisalItemSourceNameValue : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            Microsoft.Xrm.Sdk.IPluginExecutionContext context = (Microsoft.Xrm.Sdk.IPluginExecutionContext)
            serviceProvider.GetService(typeof(Microsoft.Xrm.Sdk.IPluginExecutionContext));

            if (context.Depth == 1 || context.Depth == 3)
            {
                IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService _organizationService = serviceFactory.CreateOrganizationService(context.UserId);
                Entity entity = (Entity)context.InputParameters["Target"];
                Entity postEntity = (Entity)context.PostEntityImages["entity"];

                string sourceCode = "";
                if (entity.Contains("pnl_value") || entity.Contains("pnl_displayname"))
                {
                    var fetchxml = GetFetchXml(entity);
                    var entityResults = _organizationService.RetrieveMultiple(new FetchExpression(fetchxml));
                    if (entityResults.Entities.Count == 1)
                    {
                        foreach (var o in entityResults.Entities)
                        {
                            o.Attributes["pnl_source1name"] = postEntity.Attributes["pnl_displayname"].ToString();
                            o.Attributes["pnl_source1value"] = postEntity.Attributes["pnl_value"].ToString();
                            _organizationService.Update(o);
                        }
                    }

                }

            }
        }

        private string GetFetchXml(Entity entity)
        {

            string appItem = @"<fetch distinct='false' mapping='logical'>" +
                                                "<entity name='pnl_appraisal'>" +
                                                    "<attribute name='pnl_source1name'/>" +
                                                    "<attribute name='pnl_source1value'/>" +
                                                    "<filter type='and'>" +
                                                          "<condition attribute='pnl_appraisalid' operator='eq' uitype='pnl_appraisal' value='" + entity.Attributes["pnl_appraisalid"].ToString() + "'></condition>" +
                                                          "</filter>" +
                                                        "</entity>" +
                                                       "</fetch>";
            return appItem;
        }
    }
}


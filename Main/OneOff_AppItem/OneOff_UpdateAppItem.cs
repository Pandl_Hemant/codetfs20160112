﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;


namespace OneOff_AppItem
{
    class OneOff_UpdateAppItem : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            Microsoft.Xrm.Sdk.IPluginExecutionContext context = (Microsoft.Xrm.Sdk.IPluginExecutionContext)
            serviceProvider.GetService(typeof(Microsoft.Xrm.Sdk.IPluginExecutionContext));

            if (context.Depth == 1)
            {
                IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService _organizationService = serviceFactory.CreateOrganizationService(context.UserId);
                Entity entity = (Entity)context.InputParameters["Target"];
                Entity postEntity = (Entity)context.PostEntityImages["entity"];
                if (postEntity.Contains("pnl_appraisalid"))
                {
                    EntityReference app = (EntityReference)(postEntity.Attributes["pnl_appraisalid"]);

                    Guid appraisalId = app.Id;



                    //var fetchxml = GetFetchXml(entity, sourceCode);
                    if (entity.Contains("pnl_calccode") || entity.Contains("pnl_value") || entity.Contains("pnl_displayname"))
                    {
                        string code = "SOURCE_1";
                        var fetchXml = GetFetchXml(appraisalId, code);
                        var entityResults = _organizationService.RetrieveMultiple(new FetchExpression(fetchXml));
                        if (entityResults.Entities.Count == 1)
                        {
                            foreach (var o in entityResults.Entities)
                            {
                                ColumnSet att = new ColumnSet(new string[] { "pnl_source1name" });
                                Entity appr = (Entity)_organizationService.Retrieve(app.LogicalName, app.Id, att);
                                if (appr != null)
                                {

                                    appr.Attributes["pnl_source1name"] = o.Contains("pnl_displayname") ? o.Attributes["pnl_displayname"].ToString() : null;
                                    if (o.Contains("pnl_value"))
                                    {
                                        Money value = new Money();
                                        value.Value = Convert.ToDecimal(o.Attributes["pnl_value"].ToString());
                                        appr.Attributes["pnl_source1value"] = value;
                                    }
                                    _organizationService.Update(appr);
                                }
                            }
                        }
                        code = "Source_2";
                        fetchXml = GetFetchXml(appraisalId, code);
                        entityResults = _organizationService.RetrieveMultiple(new FetchExpression(fetchXml));
                        if (entityResults.Entities.Count == 1)
                        {
                            foreach (var o in entityResults.Entities)
                            {
                                ColumnSet att = new ColumnSet(new string[] { "pnl_source2name" });
                                Entity appr = (Entity)_organizationService.Retrieve(app.LogicalName, app.Id, att);
                                if (appr != null)
                                {
                                    appr.Attributes["pnl_source2name"] = o.Contains("pnl_displayname") ? o.Attributes["pnl_displayname"].ToString() : null;
                                    if (o.Contains("pnl_value"))
                                    {
                                        Money value = new Money();
                                        value.Value = Convert.ToDecimal(o.Attributes["pnl_value"].ToString());
                                        appr.Attributes["pnl_source2value"] = value;
                                    }

                                    _organizationService.Update(appr);
                                }
                            }
                        }
                        code = "Source_3";
                        fetchXml = GetFetchXml(appraisalId, code);
                        entityResults = _organizationService.RetrieveMultiple(new FetchExpression(fetchXml));
                        if (entityResults.Entities.Count == 1)
                        {
                            foreach (var o in entityResults.Entities)
                            {
                                ColumnSet att = new ColumnSet(new string[] { "pnl_source3name" });
                                Entity appr = (Entity)_organizationService.Retrieve(app.LogicalName, app.Id, att);
                                if (appr != null)
                                {
                                    appr.Attributes["pnl_source3name"] = o.Contains("pnl_displayname") ? o.Attributes["pnl_displayname"].ToString() : null;

                                    if (o.Contains("pnl_value"))
                                    {
                                        Money value = new Money();
                                        value.Value = Convert.ToDecimal(o.Attributes["pnl_value"].ToString());
                                        appr.Attributes["pnl_source3value"] = value;
                                    }
                                    _organizationService.Update(appr);
                                }
                            }
                        }
                        code = "Source_4";
                        fetchXml = GetFetchXml(appraisalId, code);
                        entityResults = _organizationService.RetrieveMultiple(new FetchExpression(fetchXml));
                        if (entityResults.Entities.Count == 1)
                        {
                            foreach (var o in entityResults.Entities)
                            {
                                ColumnSet att = new ColumnSet(new string[] { "pnl_source4name" });
                                Entity appr = (Entity)_organizationService.Retrieve(app.LogicalName, app.Id, att);
                                if (appr != null)
                                {
                                    appr.Attributes["pnl_source4name"] = o.Contains("pnl_displayname") ? o.Attributes["pnl_displayname"].ToString() : null;
                                    if (o.Contains("pnl_value"))
                                    {
                                        Money value = new Money();
                                        value.Value = Convert.ToDecimal(o.Attributes["pnl_value"].ToString());
                                        appr.Attributes["pnl_source4value"] = value;
                                    }

                                    _organizationService.Update(appr);
                                }
                            }
                        }
                        code = "DESCRIPTION_NOTES";
                        fetchXml = GetFetchXml(appraisalId, code);
                        entityResults = _organizationService.RetrieveMultiple(new FetchExpression(fetchXml));
                        if (entityResults.Entities.Count == 1)
                        {
                            foreach (var o in entityResults.Entities)
                            {
                                ColumnSet att = new ColumnSet(new string[] { "pnl_source4name" });
                                Entity appr = (Entity)_organizationService.Retrieve(app.LogicalName, app.Id, att);
                                if (appr != null)
                                {
                                    appr.Attributes["pnl_descriptionnotes"] = o.Contains("pnl_value") ? o.Attributes["pnl_value"].ToString() : null;
                                    _organizationService.Update(appr);
                                }
                            }
                        }
                        code = "WORKSHOP_NOTES";
                        fetchXml = GetFetchXml(appraisalId, code);
                        entityResults = _organizationService.RetrieveMultiple(new FetchExpression(fetchXml));
                        if (entityResults.Entities.Count == 1)
                        {
                            foreach (var o in entityResults.Entities)
                            {
                                ColumnSet att = new ColumnSet(new string[] { "pnl_source4name" });
                                Entity appr = (Entity)_organizationService.Retrieve(app.LogicalName, app.Id, att);
                                if (appr != null)
                                {
                                    appr.Attributes["pnl_workshopnotes"] = o.Contains("pnl_value") ? o.Attributes["pnl_value"].ToString() : null;
                                    _organizationService.Update(appr);
                                }
                            }
                        }
                    }
                }
            }
        }

        private string GetFetchXml(Guid appraisalId, string code)
        {
            string appItem = @"<fetch distinct='false' mapping='logical'>" +
                                                "<entity name='pnl_appraisalitem'>" +
                                                    "<attribute name='pnl_displayname'/>" +
                                                    "<attribute name='pnl_value'/>" +
                                                    "<attribute name='pnl_calccode'/>" +
                                                    "<filter type='and'>" +
                                                          "<condition attribute='pnl_calccode' operator='eq' value='" + code + "'></condition>" +
                                                          "<condition attribute='pnl_appraisalid' operator='eq' uitype='pnl_appraisal' value='" + appraisalId + "'></condition>" +
                                                          "</filter>" +
                                                        "</entity>" +
                                                       "</fetch>";

            return appItem;

        }
    }
}


﻿namespace LandPower.Plugin.TradeIn
{
	using System;
	using System.Collections.Generic;
	using System.Linq;

	using Microsoft.Crm.Sdk.Messages;
	using Microsoft.Xrm.Sdk;

	using BusinessLogic;
	using Data;

	// Emails request to appropriate Approvers. Triggered on update of pnl_approvalrequestedon.
	public class AppraisalRequestApproval : Plugin
	{
		private LocalPluginContext _localPluginContext;

		public IOrganizationService OrganizationService { get; set; }
		public string OrganizationName { get; set; }

		public AppraisalRequestApproval()
			: base(typeof(AppraisalRequestApproval))
		{
			RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update", "pnl_appraisal", ExecuteRequestApproval));
		}

		protected void ExecuteRequestApproval(LocalPluginContext localContext)
		{
			if (localContext == null)
			{
				throw new ArgumentNullException("localContext");
			}

			var context = localContext.PluginExecutionContext;
			var inputEntity = ((Entity)context.InputParameters["Target"]).ToEntity<pnl_appraisal>();
			var postImageEntity = context.PostEntityImages["postimage"]; // Includes ownerid and Recommended Trade Price and itradephotocount.

			if (context.Depth > 3) return;

			if (inputEntity.pnl_approvalrequestedon == null) return;

			OrganizationName = context.OrganizationName;
			OrganizationService = localContext.OrganizationService;
			_localPluginContext = localContext;

			TryToRequestApproval(postImageEntity, context.InitiatingUserId);
		}

		public void TryToRequestApproval(Entity postImageEntity, Guid fromUserId)
		{
			try
			{
				RequestApproval(postImageEntity, fromUserId);
			}
			catch (Exception ex)
			{
				_localPluginContext.Trace("Inner Exception: " + ex.InnerException);
				_localPluginContext.Trace("StackTrace: " + ex.StackTrace);
				throw new InvalidPluginExecutionException(string.Format("RequestApprovalPlugin exception: {0}", ex.Message));
			}
		}

		public void RequestApproval(Entity postImageEntity, Guid fromUserId)
		{
			var xrmContext = new CrmServiceContext(OrganizationService);
			var appraisal = postImageEntity.ToEntity<pnl_appraisal>();

			if(!appraisal.HasRequiredItemValues(xrmContext))
				throw new InvalidPluginExecutionException("Please complete the value of required items.");

			if (!appraisal.HaveAllPhotosUploaded(xrmContext))
				throw new InvalidPluginExecutionException("Please ensure all photos have uploaded.");

			var approvers = appraisal.GetActivityPartiesForApproval(xrmContext).ToList();
			var primaryApprover =
				approvers.FirstOrDefault(a => a.ParticipationTypeMaskEnum == activityparty_participationtypemask.ToRecipient);
			if(null == primaryApprover) throw new InvalidPluginExecutionException("There are no Approvers available.");

			CreateAndSendEmail(approvers, appraisal, fromUserId);
			UpdatePrimaryApprover(postImageEntity.Id, primaryApprover.PartyId.Id);
			ChangeStatusToInspectionWaitingForApproval(postImageEntity.Id);
		}

		internal void UpdatePrimaryApprover(Guid appraisalId, Guid approverId)
		{
			var appraisal = new pnl_appraisal
			{
				Id = appraisalId,
				pnl_PrimaryApprover = new EntityReference("systemuser", approverId)
			};
			OrganizationService.Update(appraisal.ToEntity<Entity>());
		}

		internal void ChangeStatusToInspectionWaitingForApproval(Guid appraisalId)
		{
			var request = new SetStateRequest
			{
				EntityMoniker = new EntityReference("pnl_appraisal", appraisalId),
				State = new OptionSetValue((int)pnl_appraisalState.Active),
				Status = new OptionSetValue((int)pnl_appraisal_statuscode.InspectionWaitingforApproval)
			};
			OrganizationService.Execute(request);
		}

		public void CreateAndSendEmail(List<ActivityParty> activityParties, pnl_appraisal appraisal, Guid fromUserId)
		{
			var subject = "Approval Request for Trade-in Inspection: " + appraisal.pnl_name;
			var description = string.Format("Please review and approve the following Trade-in Inspection.<br/><br/>{0}", appraisal.BuildAppraisalLink(OrganizationName));
			var email = new Email
			{
				To = new [] { activityParties.First(a => a.ParticipationTypeMaskEnum == activityparty_participationtypemask.ToRecipient) },
				From = new [] { new ActivityParty{ PartyId = new EntityReference("systemuser", fromUserId)} },
				Cc = activityParties.Where(a => a.ParticipationTypeMaskEnum == activityparty_participationtypemask.CCRecipient),
				Subject = subject,
				Description = description,
				DirectionCode = true, // Outgoing
				RegardingObjectId = new EntityReference("pnl_appraisal", appraisal.Id)
			};
			var emailId = OrganizationService.Create(email);

			SendEmail(emailId);
		}

		internal void SendEmail(Guid emailId)
		{
			var request = new SendEmailRequest
			{
				EmailId = emailId,
				TrackingToken = "",
				IssueSend = true
			};

			OrganizationService.Execute(request);
		}
	}
} 
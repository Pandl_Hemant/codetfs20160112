﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace LandPower.Plugin.TradeIn
{
	using Microsoft.Xrm.Sdk;
	using Microsoft.Xrm.Sdk.Messages;
	using Microsoft.Xrm.Sdk.Metadata;
	using Microsoft.Xrm.Sdk.Query;

	internal class DfaHelper
	{
		private const int LanguageId = 1033;
		private Dictionary<string, string> _attributeDisplayNames;
		private Dictionary<string, Dictionary<int, string>> _optionSetDisplayValues;
		internal IOrganizationService OrganizationService { get; set; }

		public DfaHelper(IOrganizationService organizationService)
		{
			OrganizationService = organizationService;
		}

		internal void GetAttributeDisplayNames(int languageId, string entityName)
		{
			_attributeDisplayNames = new Dictionary<string, string>();
			_optionSetDisplayValues = new Dictionary<string, Dictionary<int, string>>();
			var entityMetadata = GetEntityMetadata(entityName, EntityFilters.Attributes);

			foreach (var attribute in entityMetadata.Attributes)
			{
				var displayName = attribute.DisplayName.LocalizedLabels.Count > 0
					? attribute.DisplayName.LocalizedLabels.First(x => x.LanguageCode == languageId).Label
					: string.Empty;
				_attributeDisplayNames.Add(attribute.LogicalName, displayName);

				if (attribute.AttributeType == AttributeTypeCode.Picklist || attribute.AttributeType == AttributeTypeCode.Status
					|| attribute.AttributeType == AttributeTypeCode.State)
				{
					GetOptionSetDisplayValues(entityName, attribute.LogicalName);
				}
			}
		}

		internal void GetOptionSetDisplayValues(string entityName, string attributeName)
		{
			var request = new RetrieveAttributeRequest
			{
				EntityLogicalName = entityName,
				LogicalName = attributeName,
				RetrieveAsIfPublished = true
			};
			var response = (RetrieveAttributeResponse)OrganizationService.Execute(request);
			List<OptionMetadata> optionList;
			switch (attributeName.ToLower())
			{
				case "statecode":
					var stateMetadata = (StateAttributeMetadata)response.AttributeMetadata;
					optionList = stateMetadata.OptionSet.Options.ToList();
					break;
				case "statuscode":
					var statusMetadata = (StatusAttributeMetadata)response.AttributeMetadata;
					optionList = statusMetadata.OptionSet.Options.ToList();
					break;
				default:
					var metadata = (PicklistAttributeMetadata)response.AttributeMetadata;
					optionList = metadata.OptionSet.Options.ToList();
					break;
			}

			if (!_optionSetDisplayValues.ContainsKey(attributeName))
			{
				_optionSetDisplayValues.Add(attributeName, optionList.ToDictionary(o => o.Value.GetValueOrDefault(),
					o => o.Label.LocalizedLabels.First(x => x.LanguageCode == LanguageId).Label));
			}
		}

		internal string GetEntityReferenceName(EntityReference entityReference)
		{
			if (!string.IsNullOrEmpty(entityReference.Name)) return entityReference.Name;

			var entityMetadata = GetEntityMetadata(entityReference.LogicalName, EntityFilters.Entity);
			var primaryAttributeName = entityMetadata.PrimaryNameAttribute;

			var entity = OrganizationService.Retrieve(entityReference.LogicalName, entityReference.Id, new ColumnSet(new[] { primaryAttributeName }));
			return (null == entity || !entity.Contains(primaryAttributeName))
				? string.Empty
				: entity[primaryAttributeName].ToString();
		}

		internal EntityMetadata GetEntityMetadata(string entityName, EntityFilters entityFilter)
		{
			var request = new RetrieveEntityRequest
			{
				EntityFilters = entityFilter,
				LogicalName = entityName
			};
			var response = (RetrieveEntityResponse)OrganizationService.Execute(request);

			return response.EntityMetadata;
		}

		public string GetChangedValueTable(IEnumerable<Tuple<string, object, object>> values, string entityName)
		{
			GetAttributeDisplayNames(LanguageId, entityName);
			var displayValues = new Dictionary<string, Tuple<string, string, string>>();

// ReSharper disable once LoopCanBeConvertedToQuery
			foreach (var value in values)
			{
				displayValues.Add(_attributeDisplayNames[value.Item1], new Tuple<string, string, string>(
					string.Format("{0} ({1})", _attributeDisplayNames[value.Item1], value.Item1),
					GetAttributeValue(value.Item2, value.Item1),
					GetAttributeValue(value.Item3, value.Item1)));
			}
			var sortedNames = displayValues.Keys.OrderBy(k => k);

			var result = new StringBuilder("<table>");
			result.AppendLine("<tr><td><b>Field Name:</b></td><td><b>Old Value:</b></td><td><b>New Value:</b></td></tr>");

			foreach (var key in sortedNames)
			{
				result.AppendLine(string.Format("<tr><td>{0}</td><td>{1}</td><td>{2}</td></tr>",
					displayValues[key].Item1, displayValues[key].Item2, displayValues[key].Item3));
			}

			result.AppendLine("</table>");

			return result.ToString();
		}

		internal IEnumerable<Tuple<string, object, object>> GetChangedAttributes(Entity inputEntity, Entity preImageEntity)
		{
			var result = new List<Tuple<string, object, object>>();

			// ReSharper disable once LoopCanBeConvertedToQuery
			foreach (var attributeName in inputEntity.Attributes.Keys)
			{
				if (attributeName == "modifiedonbehalfby" || attributeName == "createdonbehalfby") continue;
				var oldValue = preImageEntity != null && preImageEntity.Contains(attributeName) ? preImageEntity[attributeName] : null;
				if (!HasAttributeValueChanged(oldValue, inputEntity[attributeName])) continue;

				result.Add(new Tuple<string, object, object>(attributeName, oldValue, inputEntity[attributeName]));
			}

			return result;
		}

		internal bool HasAttributeValueChanged(object oldValue, object newValue)
		{
			if ((null == oldValue && null != newValue) || (oldValue != null && newValue == null)) return true;

			var typeName = ParseShortTypeName(oldValue);
			var hasChanged = false;

			// We can ignore guid (uniqueidentifier) since it's the record's primary key.
			switch (typeName)
			{
				case "bool":
				case "datetime":
				case "decimal":
				case "double":
				case "int":
				case "long":
				case "string":
					hasChanged = oldValue != newValue;
					break;
				// ReSharper disable PossibleNullReferenceException
				case "entityreference":
					hasChanged = ((EntityReference)oldValue).Id != ((EntityReference)newValue).Id;
					break;
				case "money":
					hasChanged = ((Money)oldValue).Value != ((Money)newValue).Value;
					break;
				case "optionsetvalue":
					hasChanged = ((OptionSetValue)oldValue).Value != ((OptionSetValue)newValue).Value;
					// ReSharper restore PossibleNullReferenceException
					break;
			}

			return hasChanged;
		}

		internal string GetAttributeValue(object value, string attributeName)
		{
			if (null == value) return string.Empty;

			var typeName = ParseShortTypeName(value);

			switch (typeName)
			{
				case "datetime":
					// ReSharper disable once SpecifyACultureInStringConversionExplicitly
					return ((DateTime)value).ToLocalTime().ToString();
				case "entityreference":
					return GetEntityReferenceName((EntityReference)value);
				case "money":
					return ((Money)value).Value.ToString(CultureInfo.InvariantCulture);
				case "optionsetvalue":
					var optionValue = ((OptionSetValue)value).Value;
					return _optionSetDisplayValues[attributeName][optionValue];
				default:
					return value.ToString();
			}
		}

		internal string ParseShortTypeName(object item)
		{
			var typeName = item.GetType().Name;
			var index = typeName.LastIndexOf('.');

			return index > 0 ? typeName.Substring(index + 1).ToLower() : typeName.ToLower();
		}
	}
}

﻿namespace LandPower.Plugin.TradeIn
{
	using System;
	
	using Microsoft.Crm.Sdk.Messages;
	using Microsoft.Xrm.Sdk;

	using BusinessLogic;
	using Data;

	// Emails request to appropriate Approvers. Triggered on update of pnl_approvalrequestedon.
	public class AppraisalApproval : Plugin
	{
		private LocalPluginContext _localPluginContext;

		public IOrganizationService OrganizationService { get; set; }
		public string OrganizationName { get; set; }

		public AppraisalApproval() : base(typeof(AppraisalApproval))
		{
			RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update", "pnl_appraisal", ExecuteAppraisalApproval));
		}

		protected void ExecuteAppraisalApproval(LocalPluginContext localContext)
		{
			if (localContext == null)
			{
				throw new ArgumentNullException("localContext");
			}

			var context = localContext.PluginExecutionContext;
			var postImageEntity = context.PostEntityImages["postimage"];

			if (context.Depth > 3) return;

			OrganizationName = context.OrganizationName;
			OrganizationService = localContext.OrganizationService;
			_localPluginContext = localContext;

			TryToApproveAppraisal(postImageEntity, context.InitiatingUserId);
		}

		public void TryToApproveAppraisal(Entity postImageEntity, Guid fromUserId)
		{
			try 
			{
				ApproveAppraisal(postImageEntity, fromUserId);
			}
			catch (Exception ex)
			{
				_localPluginContext.Trace("Inner Exception: " + ex.InnerException);
				_localPluginContext.Trace("StackTrace: " + ex.StackTrace);
				throw new InvalidPluginExecutionException(string.Format("AppraisalApprovalPlugin exception: {0}", ex.Message));
			}
		}

		public void ApproveAppraisal(Entity postImageEntity, Guid fromUserId)
		{
			var xrmContext = new CrmServiceContext(OrganizationService);
			var appraisal = postImageEntity.ToEntity<pnl_appraisal>();

			if (appraisal.pnl_ValuationApprovedOn == null) return;

			if (!appraisal.HasRequiredItemValues(xrmContext))
				throw new InvalidPluginExecutionException("Please complete the value of required items.");

			if (!appraisal.HaveAllPhotosUploaded(xrmContext))
				throw new InvalidPluginExecutionException("Please ensure all photos have uploaded.");

			var user = new SystemUser { Id = fromUserId };
			if(!user.IsTeamMember("Appraisal Approvers", xrmContext))
				throw new InvalidPluginExecutionException("Permission denied. You need to be a member of the 'Appraisal Approvers' team.");

			CreateAndSendEmail(appraisal.OwnerId.Id, appraisal, fromUserId);
			UpdateApprovedBy(postImageEntity.Id, fromUserId);
			ChangeStatusToApproved(postImageEntity.Id);
		}

		internal void UpdateApprovedBy(Guid appraisalId, Guid approverId)
		{
			var appraisal = new pnl_appraisal
			{
				Id = appraisalId,
				pnl_ValuationApprovedBy = new EntityReference("systemuser", approverId)
			};
			OrganizationService.Update(appraisal.ToEntity<Entity>());
		}

		internal void ChangeStatusToApproved(Guid appraisalId)
		{
			var request = new SetStateRequest
			{
				EntityMoniker = new EntityReference("pnl_appraisal", appraisalId),
				State = new OptionSetValue((int)pnl_appraisalState.Active),
				Status = new OptionSetValue((int)pnl_appraisal_statuscode.ApprovedApprasial_125760001)
			};
			OrganizationService.Execute(request);
		}

		public void CreateAndSendEmail(Guid toUserId, pnl_appraisal appraisal, Guid fromUserId)
		{
			var subject = "Trade-in Inspection: " + appraisal.pnl_name + " was Approved";
			var description = string.Format("The following Trade-in Inspection was approved.<br/><br/>{0}", appraisal.BuildAppraisalLink(OrganizationName));
			var email = new Email
			{
				To = new[] { new ActivityParty { PartyId = new EntityReference("systemuser", toUserId) } },
				From = new[] { new ActivityParty { PartyId = new EntityReference("systemuser", fromUserId) } },
				Subject = subject,
				Description = description,
				DirectionCode = true, // Outgoing
				RegardingObjectId = new EntityReference("pnl_appraisal", appraisal.Id)
			};
			var emailId = OrganizationService.Create(email);

			SendEmail(emailId);
		}

		internal void SendEmail(Guid emailId)
		{
			var request = new SendEmailRequest
			{
				EmailId = emailId,
				TrackingToken = "",
				IssueSend = true
			};

			OrganizationService.Execute(request);
		}
	}
}
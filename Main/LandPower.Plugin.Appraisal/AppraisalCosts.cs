﻿using LandPower.BusinessLogic;
using LandPower.Data;

namespace LandPower.Plugin.TradeIn
{
	using System;

	using Microsoft.Xrm.Sdk;

	// Calculate total cost.
	public class AppraisalCosts : Plugin
	{
		private LocalPluginContext _localPluginContext;

		public IOrganizationService OrganizationService { get; set; }

		public AppraisalCosts()
			: base(typeof(AppraisalCosts))
		{
			RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Create", "pnl_appraisal", ExecuteAppraisalCosts));
			RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update", "pnl_appraisal", ExecuteAppraisalCosts));
		}

		protected void ExecuteAppraisalCosts(LocalPluginContext localContext)
		{
			if (localContext == null)
			{
				throw new ArgumentNullException("localContext");
			}

			var context = localContext.PluginExecutionContext;
			OrganizationService = localContext.OrganizationService;
			_localPluginContext = localContext;

			if (context.IsOfflinePlayback || context.Depth > 3) return; // No need to re-calculate average valution if/when (Outlook) clients run offline.

			switch (context.MessageName.ToLower())
			{
				case "create":
					var inputEntity = (Entity)context.InputParameters["Target"];
					TryToAddAppraisalCost(inputEntity);
					break;
				case "update":
					var postEntity = context.PostEntityImages["postimage"];
					TryToUpdateAppraisalCost(postEntity);
					break;
			}			
		}

		public void TryToAddAppraisalCost(Entity inputEntity)
		{
			try
			{
				var appraisal = inputEntity.ToEntity<pnl_appraisal>();
				var totalCost = appraisal.GetTotalCost();
				var expectedRetainedMarginDollar = appraisal.GetExpectedRetainedMarginDollars();
				var calculatedTradePrice = appraisal.GetCalculatedTradePrice(0m); // Can't have any valuations until Appraisal has been created.

				SetInputAttribute(inputEntity, "pnl_totalcosts", new Money(totalCost));
				SetInputAttribute(inputEntity, "pnl_expectedretainedmargindollar", new Money(expectedRetainedMarginDollar));
				// TODO: correct spelling once HK has fixd the CRM customizations.
				SetInputAttribute(inputEntity, "pnl_calcuatedtradeprice", new Money(calculatedTradePrice));
			}
			catch (Exception ex)
			{
				_localPluginContext.Trace("Inner Exception: " + ex.InnerException);
				_localPluginContext.Trace("StackTrace: " + ex.StackTrace);
				throw new InvalidPluginExecutionException(string.Format("CalculateAppraisalCostPlugin exception: {0}", ex.Message));
			}
		}

		public void TryToUpdateAppraisalCost(Entity postEntity)
		{
			try
			{
				var doUpdate = false;
				var appraisal = postEntity.ToEntity<pnl_appraisal>();
				var totalCost = appraisal.GetTotalCost();
				var expectedRetainedMarginDollar = appraisal.GetExpectedRetainedMarginDollars();
				var calculatedTradePrice =
					appraisal.GetCalculatedTradePrice(appraisal.pnl_CalculatedAverage == null
						? 0m
						: appraisal.pnl_CalculatedAverage.Value);

				var entity = new Entity("pnl_appraisal") {Id = postEntity.Id};
				var existingTotalCost = postEntity.GetAttributeValue<Money>("pnl_totalcosts");
				if (existingTotalCost == null || existingTotalCost.Value != totalCost)
				{
					entity.Attributes.Add("pnl_totalcosts", new Money(totalCost));
					doUpdate = true;
				}
				var existingExpectedRetainedMarginDollar = postEntity.GetAttributeValue<Money>("pnl_expectedretainedmargindollar");
				if (existingExpectedRetainedMarginDollar == null || existingExpectedRetainedMarginDollar.Value != expectedRetainedMarginDollar)
				{
					entity.Attributes.Add("pnl_expectedretainedmargindollar", new Money(expectedRetainedMarginDollar));
					doUpdate = true;
				}
				var existingCalculatedTradePrice = postEntity.GetAttributeValue<Money>("pnl_calcuatedtradeprice");
				if (existingCalculatedTradePrice == null || existingCalculatedTradePrice.Value != calculatedTradePrice)
				{
					// TODO: fix spelling once HK has updated CRM customizations.
					entity.Attributes.Add("pnl_calcuatedtradeprice", new Money(calculatedTradePrice));
					doUpdate = true;
				}
				if (doUpdate)
				{
					OrganizationService.Update(entity);
				}
			}
			catch (Exception ex)
			{
				_localPluginContext.Trace("Inner Exception: " + ex.InnerException);
				_localPluginContext.Trace("StackTrace: " + ex.StackTrace);
				throw new InvalidPluginExecutionException(string.Format("CalculateAppraisalCostPlugin exception: {0}", ex.Message));
			}
		}

		internal void SetInputAttribute(Entity inputEntity, string attributeName, object attributeValue)
		{
			if (inputEntity.Attributes.Contains(attributeName))
			{
				inputEntity.Attributes[attributeName] = attributeValue;
			}
			else
			{
				inputEntity.Attributes.Add(attributeName, attributeValue);
			}
		}
	}
}
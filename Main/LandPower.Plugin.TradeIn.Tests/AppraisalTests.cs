﻿using System;
using System.Collections.Generic;
using LandPower.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;

namespace LandPower.Plugin.TradeIn.Tests
{
	[TestClass]
	public class AppraisalTests
	{
		[TestMethod]
		public void TryToUpdateAppraisalTest()
		{
			var appraisalId = new Guid("1759A22D-EF28-E411-80F4-0050568A79C5"); // Demo Appraisal.
			var appraisal = new pnl_appraisal
			{
				Id = appraisalId,
				pnl_lastsavedfromappraisalitemon = DateTime.Now
			};
			var preImageAppraisal = new pnl_appraisal
			{
				Id = appraisalId,
				pnl_CalcuatedTradePrice = new Money(26450m),
				pnl_ExpectedRetainedMarginDollar = new Money(3839.88m),
				pnl_ExpectedRetainedMarginPercentage = 12m,
				pnl_Freight = new Money(249),
				pnl_OtherCosts = new Money(369m),
				pnl_PreDeliveryExpenses = new Money(446m),
				pnl_RecommededListPrice = new Money(31999m),
				pnl_RepairCosts = new Money(879m),
				pnl_TotalCosts = new Money(1943m)
			};

			var target = new Appraisal {OrganizationService = GetOrganizationService()};
			target.TryToUpdateAppraisal(appraisal.ToEntity<Entity>(), preImageAppraisal.ToEntity<Entity>());

			//Assert.Inconclusive("Method does not return a value.");
		}

		[TestMethod]
		public void CreateAndSendEmailTest()
		{
			var appraisalId = new Guid("5467084C-5D3D-E411-80F4-0050568A79C5");
			var toUserId = new Guid("A7903ACC-640F-E311-8BEA-1CC1DE79B4CA"); // Peter Marshall
			var fromUserId = new Guid("897F2015-E331-E211-9DF3-1CC1DE79B4CA"); //HemantK
			var appraisal = new pnl_appraisal
			{
				Id = appraisalId,
				pnl_name = "Kevin Contracting - 16/09/14 - Tractor (Bette)"
			};
			var approvers = new List<ActivityParty>
			{
				new ActivityParty
				{
					PartyId = new EntityReference("systemuser", toUserId),
					ParticipationTypeMaskEnum = activityparty_participationtypemask.ToRecipient
				}
			};

			var target = new AppraisalRequestApproval
			{
				OrganizationName = "idealdev",
				OrganizationService = GetOrganizationService()
			};

			target.CreateAndSendEmail(approvers, appraisal, fromUserId);

			//Assert.Inconclusive("Method does not return a value.");
		}

		[TestMethod]
		public void ExecuteRequestApprovalTest()
		{
			var appraisalId = new Guid("BFC2ED0B-2D3D-E411-80F4-0050568A79C5"); //Test Costs
			var appraisal = new pnl_appraisal
			{
				Id = appraisalId,
				pnl_approvalrequestedon = DateTime.Now
			};
			var organizationService = GetOrganizationService();
			organizationService.Update(appraisal.ToEntity<Entity>());
		}

		[TestMethod]
		public void ExecuteAppraisalApprovalTest()
		{
			var appraisalId = new Guid("bfc2ed0b-2d3d-e411-80f4-0050568a79c5"); // John Deere : 22:09:14 : <Not Specified>
			var appraisal = new pnl_appraisal
			{
				Id = appraisalId,
				pnl_ValuationApprovedOn = DateTime.Now
			};
			var organizationService = GetOrganizationService();
			organizationService.Update(appraisal.ToEntity<Entity>());
		}

		[TestMethod]
		public void UpdateAppraisalItemValueTest()
		{
			//var appraisalItemId = new Guid("FA3B3C93-F428-E411-80F4-0050568A79C5"); // TradeMe valuation on Demo Appraisal.
			var appraisalItemId = new Guid("7B1BF8DD-F228-E411-80F4-0050568A79C5"); // Serial number specification on CLAAS : 18:08:14 : Tractor.
			var item = new pnl_appraisalitem
			{
				Id = appraisalItemId,
				//pnl_Value = "28569"
				pnl_Value = "1122334456677"
			};
			var organizationService = GetOrganizationService();
			organizationService.Update(item.ToEntity<Entity>());
		}

		[TestMethod]
		public void ClearAppraisalRequestApprovedOnTest()
		{
			var appraisalId = new Guid("BFC2ED0B-2D3D-E411-80F4-0050568A79C5");
			var appraisal = new pnl_appraisal {Id = appraisalId, pnl_approvalrequestedon = null};
			var organizationService = GetOrganizationService();
			organizationService.Update(appraisal.ToEntity<Entity>());
		}

		[TestMethod]
		public void ActualRetainedMarginPercentageTest()
		{
			var appraisalId = new Guid("0E00ED32-BC28-479D-81DE-D748865A9D8D");
			var appraisal = new pnl_appraisal {Id = appraisalId, pnl_ApprovedTradePrice = new Money(25469m)};
			var organizationService = GetOrganizationService();
			organizationService.Update(appraisal.ToEntity<Entity>());
		}

		internal IOrganizationService GetOrganizationService()
		{
			const string connectionString = "Url=https://idealdev.landpower.co.nz; Username=hemantk@landpower; Password=Pass@word;";
			var crmConnection = CrmConnection.Parse(connectionString);
			return new OrganizationService(crmConnection);
		}
	}
}

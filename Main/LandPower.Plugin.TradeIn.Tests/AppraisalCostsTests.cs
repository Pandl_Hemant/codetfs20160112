﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using Microsoft.Xrm.Sdk;

namespace LandPower.Plugin.TradeIn.Tests
{
	using Moq;
	
	[TestClass]
	public class AppraisalCostsTests
	{
		[TestMethod]
		public void TryToUpdateAppraisalCostTest()
		{
			// This exists to help with debugging.
			var appraisal = new Entity("pnl_appraisal");
			var target = new AppraisalCosts {OrganizationService = new Mock<IOrganizationService>().Object};

			target.TryToUpdateAppraisalCost(appraisal);
		}
	}
}

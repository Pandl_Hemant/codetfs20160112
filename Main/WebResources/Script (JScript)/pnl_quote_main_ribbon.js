function LostQuoteEnableRule() {
    return CreateOrderEnableRule();
}

function CreateOrderIsQuoteActiveOrWon(recordId) {
    var cols = ["statecode"];
    var retrievedQuote = XrmServiceToolkit.Soap.Retrieve("quote", recordId, cols);
    var objState = retrievedQuote.attributes["statecode"];
    if (objState != undefined && objState.value != null) {
        if (objState.value == 1 || objState.value == 2) {
             return true;
        }
    }
    return false;
}

function CreateOrderEnableRule() {
    if(Xrm.Page.data == undefined || Xrm.Page.data == null) {
          return true;
    }

    var quoteid = Xrm.Page.data.entity.getId();

    if(quoteid == undefined || quoteid == null) {
          return true;
    }

    var fetchXml =
            "<fetch mapping='logical' aggregate='true'>" +
               "<entity name='quote'>" +
                  //"<attribute name='quoteid' />" +
                  "<filter type='and'>" +
                        "<condition attribute='quoteid' operator='eq' uiname='' uitype='quote' value='" + quoteid + "' />" +
                    "</filter>" +
                  "<link-entity name='salesorder' from='quoteid' to='quoteid' alias='ac'>" +
                    "<attribute name='salesorderid' aggregate='count' alias='count' />" +
                    "<filter type='and'>" +
                        "<condition attribute='statecode' operator='eq' value='0' />" +
                    "</filter>" +
                   "</link-entity>" +
                "</entity>" +
            "</fetch>";

    var fetchedContacts = XrmServiceToolkit.Soap.Fetch(fetchXml);
    if (fetchedContacts.length > 0) {
        return fetchedContacts[0].attributes['count'].formattedValue == 0;
    }
    return true;
}


function LostQuotePopulateDynamicMenu(commandProperties) {
    // Dynamically return the menu to show - this can be built at run time
    var LostQuoteMenus = '<Menu Id="LostQuote.Menu">' +
								'<MenuSection Id="LostQuote.MenuSection" Sequence="10">' +
									'<Controls Id="LostQuote.Controls">' +
                                        '<Button Id="ClosedCustomerDraft" Command="pnl.Form.quote.MainTab.Action.LostQuoteFlyOut.ItemClicked.Command" Sequence="31" LabelText="Closed � Customer Draft" />' +
										'<Button Id="ClosedBrandLoyalty" Command="pnl.Form.quote.MainTab.Action.LostQuoteFlyOut.ItemClicked.Command" Sequence="30" LabelText="Lost � Brand Loyalty" />' +
										'<Button Id="ClosedDealerLoyalty" Command="pnl.Form.quote.MainTab.Action.LostQuoteFlyOut.ItemClicked.Command" Sequence="32" LabelText="Lost � Dealer Loyalty" />' +
										'<Button Id="ClosedPrice" Command="pnl.Form.quote.MainTab.Action.LostQuoteFlyOut.ItemClicked.Command" Sequence="33" LabelText="Lost � Price" />' +
										'<Button Id="ClosedSpecifications" Command="pnl.Form.quote.MainTab.Action.LostQuoteFlyOut.ItemClicked.Command" Sequence="34" LabelText="Lost � Specifications" />' +
									'</Controls>' +
								'</MenuSection>' +
							'</Menu>';

    commandProperties["PopulationXML"] = LostQuoteMenus;
}

function LostQuoteItemClicked(commandProperties) {
    var entityId = Xrm.Page.data.entity.getId();
    if (entityId != null) {
        var entityName = 'quote';
        var LostQuoteType = commandProperties.SourceControlId;
        var ActionUniqueName = 'pnl_LostQuoteAction';
        LostQuoteExecuteAction(entityId, entityName, LostQuoteType, ActionUniqueName);
        Mscrm.ReadFormUtilities.openInSameFrame(window._etc, Xrm.Page.data.entity.getId());
    }
}

function LostQuoteExecuteAction(EntityId, EntityName, LostQuoteType, ActionUniqueName) {
    // Creating the request XML for calling the Action
    var requestXML = ""
    requestXML += "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">";
    requestXML += "  <s:Body>";
    requestXML += "    <Execute xmlns=\"http://schemas.microsoft.com/xrm/2011/Contracts/Services\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\">";
    requestXML += "      <request xmlns:a=\"http://schemas.microsoft.com/xrm/2011/Contracts\">";
    requestXML += "        <a:Parameters xmlns:b=\"http://schemas.datacontract.org/2004/07/System.Collections.Generic\">";
    requestXML += "          <a:KeyValuePairOfstringanyType>";
    requestXML += "            <b:key>Target</b:key>";
    requestXML += "            <b:value i:type=\"a:EntityReference\">";
    requestXML += "              <a:Id>" + EntityId + "</a:Id>";
    requestXML += "              <a:LogicalName>" + EntityName + "</a:LogicalName>";
    requestXML += "              <a:Name i:nil=\"true\" />";
    requestXML += "            </b:value>";
    requestXML += "          </a:KeyValuePairOfstringanyType>";
    requestXML += "          <a:KeyValuePairOfstringanyType>";
    requestXML += "            <b:key>LostQuoteType</b:key>";
    requestXML += "            <b:value i:type=\"c:string\" xmlns:c=\"http://www.w3.org/2001/XMLSchema\">" + LostQuoteType + "</b:value>";
    requestXML += "          </a:KeyValuePairOfstringanyType>";
    requestXML += "        </a:Parameters>";
    requestXML += "        <a:RequestId i:nil=\"true\" />";
    requestXML += "        <a:RequestName>" + ActionUniqueName + "</a:RequestName>";
    requestXML += "      </request>";
    requestXML += "    </Execute>";
    requestXML += "  </s:Body>";
    requestXML += "</s:Envelope>";
    var req = new XMLHttpRequest();
    req.open("POST", GetClientUrl(), false)
    req.setRequestHeader("Accept", "application/xml, text/xml, */*");
    req.setRequestHeader("Content-Type", "text/xml; charset=utf-8");
    req.setRequestHeader("SOAPAction", "http://schemas.microsoft.com/xrm/2011/Contracts/Services/IOrganizationService/Execute");
    req.send(requestXML);
    //Get the Resonse from the CRM Execute method
    var response = req.responseXML;
    return response;
}

function GetClientUrl() {
    if (typeof Xrm.Page.context == "object") {
        clientUrl = Xrm.Page.context.getClientUrl();
    }
    var ServicePath = "/XRMServices/2011/Organization.svc/web";
    return clientUrl + ServicePath;
}
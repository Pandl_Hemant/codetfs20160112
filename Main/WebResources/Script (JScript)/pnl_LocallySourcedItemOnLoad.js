var isLoaded = false;

function onLoad() {
    var CRM_FORM_TYPE = Xrm.Page.ui.getFormType();
    if (CRM_FORM_TYPE == 1) {
        preLoadMachine();
        isLocallySourced();
    } else {
        var statecode = Xrm.Page.getAttribute('statecode').getValue();
        if (statecode != 1) {
            var isdraft = QuoteIsDraft_Onload();
            if (isdraft) {
                preLoadMachine();
                isLocallySourced();
            }
            else {
                DisableControls(true);
            }
        }
    }
}

function LocallySource_OnSave() {
    try {
        var entityId = Xrm.Page.getAttribute('pnl_quote').getValue();
        if (entityId != null) {
            //    var entityName = 'quote';
            //    var ActionUniqueName = 'pnl_ExecuteQuotePlugin2';
            //    ExecuteAction(entityId[0].id, entityName, ActionUniqueName);
            //    ExecuteAction(entityId[0].id, entityName, ActionUniqueName);

            //setTimeout(function () { try { window.parent.opener.document.location.reload(true); } catch (err) { alert(err.message);} }, 2000);
            var CRM_FORM_TYPE = Xrm.Page.ui.getFormType();
            //&& window.parent != undefined && window.parent != null 
            if (CRM_FORM_TYPE == 1) {
                //window.parent.opener.document.location.reload(true);
                //window.location.reload(true);
                //isLoaded = true;
                //    window.location.reload(true);
                Xrm.Page.data.save().then(
                function () {
                    //window.parent.opener.document.location.reload(true);
                    try {
                        //document.parentWindow.parent.reload(true);
                        //window.top.parent.location.reload(true);
                        window.parent.opener.document.location.reload(true);
                        setTimeout(function () { Xrm.Page.ui.close(); }, 2000);
                    } catch (err) {
                        alert(err.message);
                    }
                    //window.top.opener.location.reload();
                },
                function () {
                    alert("Save Fail");
                });
            }
            else {
                Xrm.Page.data.entity.save("saveandclose");
            }
            
        }
    }
    catch (err) {
        alert(err.message);
    }
}

function ExecuteAction(EntityId, EntityName, ActionUniqueName) {
    // Creating the request XML for calling the Action
    var requestXML = "";
    requestXML += "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">";
    requestXML += "  <s:Body>";
    requestXML += "    <Execute xmlns=\"http://schemas.microsoft.com/xrm/2011/Contracts/Services\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\">";
    requestXML += "      <request xmlns:a=\"http://schemas.microsoft.com/xrm/2011/Contracts\">";
    requestXML += "        <a:Parameters xmlns:b=\"http://schemas.datacontract.org/2004/07/System.Collections.Generic\">";
    requestXML += "          <a:KeyValuePairOfstringanyType>";
    requestXML += "            <b:key>Target</b:key>";
    requestXML += "            <b:value i:type=\"a:EntityReference\">";
    requestXML += "              <a:Id>" + EntityId + "</a:Id>";
    requestXML += "              <a:LogicalName>" + EntityName + "</a:LogicalName>";
    requestXML += "              <a:Name i:nil=\"true\" />";
    requestXML += "            </b:value>";
    requestXML += "          </a:KeyValuePairOfstringanyType>";
    requestXML += "        </a:Parameters>";
    requestXML += "        <a:RequestId i:nil=\"true\" />";
    requestXML += "        <a:RequestName>" + ActionUniqueName + "</a:RequestName>";
    requestXML += "      </request>";
    requestXML += "    </Execute>";
    requestXML += "  </s:Body>";
    requestXML += "</s:Envelope>";

    var req = new XMLHttpRequest();
    req.open("POST", GetClientUrl(), true);
    req.setRequestHeader("Accept", "application/xml, text/xml, */*");
    req.setRequestHeader("Content-Type", "text/xml; charset=utf-8");
    req.setRequestHeader("SOAPAction", "http://schemas.microsoft.com/xrm/2011/Contracts/Services/IOrganizationService/Execute");
    req.send(requestXML);
    //alert(req.responseText);
    //Get the Resonse from the CRM Execute method
    var response = req.responseXML;
    //alert(req.status);
    return response;
}

function GetClientUrl() {
    if (typeof Xrm.Page.context == "object") {
        clientUrl = Xrm.Page.context.getClientUrl();
    }
    var ServicePath = "/XRMServices/2011/Organization.svc/web";
    return clientUrl + ServicePath;
}


function QuoteIsDraft() {
    var CRM_FORM_TYPE = Xrm.Page.ui.getFormType();
    var result = false;

    if (CRM_FORM_TYPE == 1) {
        result = true;
    }
    else {
        try{
            var quoteid = GetQuoteId();
            if (quoteid != null) {
                var cols = ["statecode", "statuscode"];
                var retrievedQuote = XrmServiceToolkit.Soap.Retrieve("quote", quoteid, cols);
                var objState = retrievedQuote.attributes["statecode"];
                var objStatus = retrievedQuote.attributes["statuscode"];

                if (objState != undefined && objState != null && objState.value != null && objStatus != undefined && objStatus != null && objStatus.value != null) {
                    if (objState.value == 0 && objStatus.value == 1) {
                        result = true;
                    }
                }
            }
        } catch (err) {
            alert(err.message);
        }
    }
    return result;
}

function GetQuoteId() {
    var cols = ["pnl_quote"];
    var recordId = Xrm.Page.data.entity.getId();
    var retrievedQuote = XrmServiceToolkit.Soap.Retrieve("pnl_locallysourceditem", recordId, cols);
    var quote = retrievedQuote.attributes["pnl_quote"];
    if (quote != undefined && quote != null && quote.id != undefined && quote.id != null) {
        return quote.id;
    }
    return null;
}

function QuoteIsDraft_Onload() {
    var CRM_FORM_TYPE = Xrm.Page.ui.getFormType();
    var result = false;
    var statecode = Xrm.Page.getAttribute('statecode').getValue();
    if (statecode != 1) {
        if (CRM_FORM_TYPE == 1) {
            result = true;
        }
        else {
            var quoteid = Xrm.Page.getAttribute('pnl_quote').getValue();
            if (quoteid != null && quoteid.length > 0 && quoteid[0].id != null) {
                var cols = ["statecode", "statuscode"];
                var retrievedQuote = XrmServiceToolkit.Soap.Retrieve("quote", quoteid[0].id, cols);
                var objState = retrievedQuote.attributes["statecode"];
                var objStatus = retrievedQuote.attributes["statuscode"];

                if (objState != undefined && objState != null && objState.value != null && objStatus != undefined && objStatus != null && objStatus.value != null) {
                    if (objState.value == 0 && objStatus.value == 1) {
                        result = true;
                    }
                }
            }
        }
    }
    return result;
}

function DisableControls(disablestatus) {
    Xrm.Page.ui.controls.get().forEach(function (control, index) {
        if (control.getDisabled() != disablestatus) {
            control.setDisabled(disablestatus);
        }
    });
}

function NoChargeItem_OnChange() {
    Xrm.Page.getControl('pnl_nochargeitem').setFocus();
    window.setTimeout(NoChargeItem, 10);
}

function NoChargeItem() {
    var noChargeItem = Xrm.Page.getAttribute('pnl_nochargeitem').getValue();
    if (noChargeItem) {
        Xrm.Page.getAttribute('pnl_discountpercentage').setSubmitMode('always');
        Xrm.Page.getAttribute('pnl_discountpercentage').setValue(100);
        Xrm.Page.getAttribute('pnl_listprice').setSubmitMode('always');
        Xrm.Page.getAttribute('pnl_listprice').setValue(0);
    } else {
        Xrm.Page.getAttribute('pnl_discountpercentage').setValue(null);
        Xrm.Page.getAttribute('pnl_listprice').setValue(null);
    }
    isLocallySourced();
}

function isLocallySourced() {
    var listPrice = Xrm.Page.getAttribute('pnl_listprice');
	var discount = Xrm.Page.getAttribute('pnl_discountpercentage');
	var noChargeItem = Xrm.Page.getAttribute('pnl_nochargeitem').getValue();

	if(listPrice != null)
	{
		if (!noChargeItem) {
		    var dealerNetPrice = Xrm.Page.getAttribute('pnl_locallysourceddnp');
		    var newValue = listPrice.getValue();

		    if (newValue != null && discount.getValue() != null) {
		        if (discount != null) {
		            newValue = listPrice.getValue() * ((100 - discount.getValue()) / 100);
		        }

		        dealerNetPrice.setValue(newValue);
		    }
		    else {
		        if (newValue != null) {
		            dealerNetPrice.setValue(newValue);
		        }
		    }
		}
	}
	
	if (noChargeItem) {
	    Xrm.Page.getControl('pnl_listprice').setDisabled(true);
	    Xrm.Page.getControl('pnl_discountpercentage').setDisabled(true);
	} else {
	    Xrm.Page.getControl('pnl_listprice').setDisabled(false);
	    Xrm.Page.getControl('pnl_discountpercentage').setDisabled(false);
	}
}

function preLoadMachine() {
    var itemLookup = Xrm.Page.getAttribute('pnl_locallysourcedmachine').getValue();
    var quote = Xrm.Page.getAttribute('pnl_quote').getValue();
    var formtype = Xrm.Page.ui.getFormType();
    var CREATE = 1;
    if (itemLookup == null && formtype == CREATE && quote != null) {
        var fetchXml =
                    "<fetch mapping='logical'>" +
                       "<entity name='pnl_locallysourcedmachine'>" +
                          "<attribute name='pnl_locallysourcedmachineid' />" +
                          "<attribute name='pnl_name' />" +
                          "<filter>" +
                             "<condition attribute='pnl_quoteid' operator='eq' value='" + quote[0].id + "' />" +
                          "</filter>" +
                       "</entity>" +
                    "</fetch>";

        var retrievedItem = XrmServiceToolkit.Soap.Fetch(fetchXml);

        if (retrievedItem.length == 1 && Xrm.Page.getAttribute("pnl_locallysourcedmachine").getValue() == null) {
            var locallyid = retrievedItem[0].attributes['pnl_locallysourcedmachineid'].value;
            var locallyname = retrievedItem[0].attributes['pnl_name'].value;
            Xrm.Page.getAttribute("pnl_locallysourcedmachine").setValue([{ id: locallyid, name: locallyname, entityType: "pnl_locallysourcedmachine" }]);
        }
    }
}
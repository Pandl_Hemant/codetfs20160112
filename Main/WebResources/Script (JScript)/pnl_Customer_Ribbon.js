/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
System: Landpower MSCRM 2013
Author: P&L Limted

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

function btnAddQuote() {
    try {
        Xrm.Page.ui.setFormNotification("Creating Quote, Please Wait...", "WARNING","1");
        window.setTimeout(btnAddQuote2,10)
    }
    catch (err) {
        Xrm.Page.ui.clearFormNotification("1");
        alert(err.message);
    }
};

function btnAddQuote2() {
    try {
        Xrm.Page.ui.controls.forEach(function (control, index) { if (control.getControlType() != "subgrid") control.setDisabled(true); });
        window.setTimeout(btnAddQuote3,10)
    }
    catch (err) {
        Xrm.Page.ui.clearFormNotification("1");
        alert(err.message);
    }
};

function btnAddQuote3() {
    try {
        // compile SOAP create object
        var createEntity = new XrmServiceToolkit.Soap.BusinessEntity("quote");
        //createEntity.attributes["name"] = "" + Xrm.Page.getAttribute("name").getValue() + " - " + new Date();
        createEntity.attributes["ownerid"] = { id: Xrm.Page.context.getUserId(), logicalName: "systemuser", type: "EntityReference" };
        createEntity.attributes["customerid"] = { id: Xrm.Page.data.entity.getId(), logicalName: Xrm.Page.data.entity.getEntityName(), type: "EntityReference" };

        // create the new record
        var createEntityID = XrmServiceToolkit.Soap.Create(createEntity);
        // open new window with new record
        if (createEntityID != "" && createEntityID != null) {
            Xrm.Utility.openEntityForm("quote", createEntityID);
        } else {
            alert("Error: Record was not created.");
        }
    }
    catch (err) {
        Xrm.Page.ui.clearFormNotification("1");
        alert(err.message);
    }
};

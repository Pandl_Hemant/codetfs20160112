﻿function RecalculateWorksheet() {
    // OnChange fields
    // pnl_otherdealtercostsfreight
    // pnl_otherdealtercostspredelivery
    // pnl_otherdealterinterestsubsidy
    // pnl_scenario1grossprofitpercentage
    // pnl_tradeinmachine1scenario2value
    // pnl_tradeinmachine1scenario3value
    // pnl_tradeinmachine2scenario2value
    // pnl_tradeinmachine2scenario3value
    // pnl_tradeinmachine3scenario2value
    // pnl_tradeinmachine3scenario3value
    // pnl_scenario2changeoverpriceexclgst
    // pnl_scenario3changeoverpriceexclgst
    // pnl_salesaidrequested
    // pnl_salesaidapproved
    debugger;
    var totalOtherDealerCosts =
        GetDecimalValue("pnl_otherdealercostsfreight") +
        GetDecimalValue("pnl_otherdealercostspredelivery") +
        GetDecimalValue("pnl_otherdealerinterestsubsidy");
    SetDecimalValue("pnl_totalotherdealercosts", totalOtherDealerCosts);

    var subtotalDealerNetPrice = GetDecimalValue("pnl_subtotaldealernetprice");
    var totalLocallySourcedItemsDealerNetPrice = GetDecimalValue("pnl_totallocallysourceditemsdealernetprice");
    var scenario1TotalDealerNetCost =
        subtotalDealerNetPrice +
        totalLocallySourcedItemsDealerNetPrice +
        totalOtherDealerCosts;
    SetDecimalValue("pnl_scenario1totaldealernetcost", scenario1TotalDealerNetCost);

    var salesAid = 0.0;
    if(ValueIsNull("pnl_salesaidapproved")){
        salesAid = GetDecimalValue("pnl_salesaidrequested");
    }
    else {
        salesAid = GetDecimalValue("pnl_salesaidapproved");
    }
        
    var scenarios2and3TotalDealerNetCost =
        subtotalDealerNetPrice +
        totalLocallySourcedItemsDealerNetPrice +
        totalOtherDealerCosts -
        salesAid;
    SetDecimalValue("pnl_scenario2totaldealernetcost", scenarios2and3TotalDealerNetCost);
    SetDecimalValue("pnl_scenario3totaldealernetcost", scenarios2and3TotalDealerNetCost);

    var scenario1GrossProfitProportion = GetDecimalValue("pnl_scenario1grossprofitpercentage") / 100.00;
    var scenario1GrossProfitDollar = 0.0;
    if (scenario1GrossProfitProportion != 0.0)
    {
        scenario1GrossProfitDollar = (GetDecimalValue("pnl_scenario1totaldealernetcost") / (1 - scenario1GrossProfitProportion))
            * scenario1GrossProfitProportion;
    }
    SetDecimalValue("pnl_scenario1grossprofitdollar", scenario1GrossProfitDollar);

    var scenario1NetSellingPrice =
        scenario1TotalDealerNetCost +
        scenario1GrossProfitDollar;
    SetDecimalValue("pnl_scenario1netsellingprice", scenario1NetSellingPrice);

    var scenario2TotalTradedValue =
        GetDecimalValue("pnl_tradeinmachine1scenario2value") +
        GetDecimalValue("pnl_tradeinmachine2scenario2value") +
        GetDecimalValue("pnl_tradeinmachine3scenario2value");
    SetDecimalValue("pnl_scenario2totaltradedvalue", scenario2TotalTradedValue);

    var scenario3TotalTradedValue =
        GetDecimalValue("pnl_tradeinmachine1scenario3value") +
        GetDecimalValue("pnl_tradeinmachine2scenario3value") +
        GetDecimalValue("pnl_tradeinmachine3scenario3value");
    SetDecimalValue("pnl_scenario3totaltradedvalue", scenario3TotalTradedValue);

    var totalBookValue = GetDecimalValue("pnl_totalbookvalue");
    var scenario2OvertradeAmount = scenario2TotalTradedValue - totalBookValue;
    SetDecimalValue("pnl_scenario2overtradeamount", scenario2OvertradeAmount);

    var scenario3OvertradeAmount = scenario3TotalTradedValue - totalBookValue;
    SetDecimalValue("pnl_scenario3overtradeamount", scenario3OvertradeAmount);

    var scenario1ChangeoverPriceExclGST = scenario1NetSellingPrice - totalBookValue;
    SetDecimalValue("pnl_scenario1changeoverpriceexclgst", scenario1ChangeoverPriceExclGST);

    var scenario2ChangeoverPriceExclGST = GetDecimalValue("pnl_scenario2changeoverpriceexclgst");
    if (scenario2ChangeoverPriceExclGST == 0.0)
    {
        scenario2ChangeoverPriceExclGST = scenario1ChangeoverPriceExclGST;
        SetDecimalValue("pnl_scenario2changeoverpriceexclgst", scenario1ChangeoverPriceExclGST);
    }

    var scenario3ChangeoverPriceExclGST = GetDecimalValue("pnl_scenario3changeoverpriceexclgst");
    if (scenario3ChangeoverPriceExclGST == 0.0)
    {
        scenario3ChangeoverPriceExclGST = scenario1ChangeoverPriceExclGST;
        SetDecimalValue("pnl_scenario3changeoverpriceexclgst", scenario1ChangeoverPriceExclGST);
    }

    var totalListPrice = GetDecimalValue("pnl_totallistprice")
    var scenario2ChangeoverDiscounts = totalListPrice - scenario2TotalTradedValue - scenario2ChangeoverPriceExclGST;
    SetDecimalValue("pnl_scenario2changeoverdiscounts", scenario2ChangeoverDiscounts);

    var scenario3ChangeoverDiscounts = totalListPrice - scenario3TotalTradedValue - scenario3ChangeoverPriceExclGST;
    SetDecimalValue("pnl_scenario3changeoverdiscounts", scenario3ChangeoverDiscounts);

    var scenario2NetSellingPrice = totalListPrice - scenario2ChangeoverDiscounts;
    SetDecimalValue("pnl_scenario2netsellingprice", scenario2NetSellingPrice);

    var scenario3NetSellingPrice = totalListPrice - scenario3ChangeoverDiscounts;
    SetDecimalValue("pnl_scenario3netsellingprice", scenario3NetSellingPrice);

    SetDecimalValue("pnl_estimatedrevenue", scenario3NetSellingPrice);

    var scenario2NetSellingPriceInternal = scenario2NetSellingPrice - scenario2OvertradeAmount;
    SetDecimalValue("pnl_scenario2netsellingpriceinternal", scenario2NetSellingPriceInternal);

    var scenario3NetSellingPriceInternal = scenario3NetSellingPrice - scenario3OvertradeAmount;
    SetDecimalValue("pnl_scenario3netsellingpriceinternal", scenario3NetSellingPriceInternal);

    var scenario2GrossProfitDollar = (scenario2NetSellingPriceInternal - scenarios2and3TotalDealerNetCost);
    SetDecimalValue("pnl_scenario2grossprofitdollar", scenario2GrossProfitDollar);

    var scenario3GrossProfitDollar = scenario3NetSellingPriceInternal - scenarios2and3TotalDealerNetCost;
    SetDecimalValue("pnl_scenario3grossprofitdollar", scenario3GrossProfitDollar);

    if (scenario2NetSellingPriceInternal == 0.0)
        SetDecimalValue("pnl_scenario2grossprofitpercentage", 0.0);
    else
        SetDecimalValue("pnl_scenario2grossprofitpercentage",
            (100.0 * scenario2GrossProfitDollar / scenario2NetSellingPriceInternal));

    if (scenario3NetSellingPriceInternal == 0.0)
        SetDecimalValue("pnl_scenario3grossprofitpercentage", 0.0);
    else
        SetDecimalValue("pnl_scenario3grossprofitpercentage", 
            (100.0 * scenario3GrossProfitDollar / scenario3NetSellingPriceInternal));

    var scenario1ChangeoverDiscounts = totalListPrice - scenario1NetSellingPrice;
    SetDecimalValue("pnl_scenario1changeoverdiscounts", scenario1ChangeoverDiscounts);

    if (scenario1NetSellingPrice == 0.0)
        SetDecimalValue("pnl_scenario1changeovergrossprofitpercentage", 0.0);
    else
        SetDecimalValue("pnl_scenario1changeovergrossprofitpercentage",
            (100.0 * (scenario1NetSellingPrice - scenario1TotalDealerNetCost) / scenario1NetSellingPrice));

    // 6g
    var s2Denominator = scenario2NetSellingPrice - scenario2OvertradeAmount;
    if(s2Denominator == 0.0)
        SetDecimalValue("pnl_scenario2changeovergrossprofitpercentage", 0.0);
    else
        SetDecimalValue("pnl_scenario2changeovergrossprofitpercentage", (100.0 * (scenario2NetSellingPrice - scenario2OvertradeAmount - scenarios2and3TotalDealerNetCost) / s2Denominator));

    // 6j
    var s3Denominator = scenario3NetSellingPrice - scenario3OvertradeAmount;
    if (s3Denominator == 0.0)
        SetDecimalValue("pnl_scenario3changeovergrossprofitpercentage", 0.0);
    else
        SetDecimalValue("pnl_scenario3changeovergrossprofitpercentage", (100.0 * (scenario3NetSellingPrice - scenario3OvertradeAmount - scenarios2and3TotalDealerNetCost) / s3Denominator));
}

function GetDecimalValue(editName) {
    var sa = Xrm.Page.getAttribute(editName);

    if (sa == null) {
        alert("Recalc GetDecimalValue() failed to find control: " + editName);
        return 0.0;
    }

    if (!sa.getValue()) {
        return 0.0;
    }

    return sa.getValue();
}

function ValueIsNull(editName) {
    var sa = Xrm.Page.getAttribute(editName);

    if (sa == null) {
        alert("Recalc GetDecimalValue() failed to find control: " + editName);
        return true;
    }

    if (!sa.getValue()) {
        return true;
    }

    return false;
}

function SetDecimalValue(editName, newValue) {
    var sa = Xrm.Page.getAttribute(editName);

    if (sa == null) {
        alert("Recalc SetDecimalValue() failed to find control: " + editName);
        return;
    }

    sa.setValue(newValue);
}
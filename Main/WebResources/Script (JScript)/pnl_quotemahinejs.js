﻿function onLoad() {
    var isdraft = QuoteIsDraft_Onload();
    if (!isdraft) {
        DisableControls(true);
    }
    preFilterLookup_config();
    preFilterLookup();
}

function DisableControls(disablestatus) {
    Xrm.Page.ui.controls.get().forEach(function (control, index) {
        if (control.getDisabled() != disablestatus) {
            control.setDisabled(disablestatus);
        }
    });
}
function GetQuoteId() {
    var cols = ["pnl_quoteid"];
    var recordId = Xrm.Page.data.entity.getId();
    var retrievedQuote = XrmServiceToolkit.Soap.Retrieve("pnl_quotemachine", recordId, cols);
    var quote = retrievedQuote.attributes["pnl_quoteid"];
    if (quote != undefined && quote != null && quote.id != undefined && quote.id != null) {
        return quote.id;
    }
    return null;
}
function QuoteIsDraft_Onload() {
    var CRM_FORM_TYPE = Xrm.Page.ui.getFormType();
    var result = false;
    var statecode = Xrm.Page.getAttribute('statecode').getValue();
    if (statecode != 1) {
        if (CRM_FORM_TYPE == 1) {
            result = true;
        }
        else {
            var quoteid = Xrm.Page.getAttribute('pnl_quoteid').getValue();
            if (quoteid != null && quoteid.length > 0 && quoteid[0].id != null) {
                var cols = ["statecode", "statuscode"];
                var retrievedQuote = XrmServiceToolkit.Soap.Retrieve("quote", quoteid[0].id, cols);
                var objState = retrievedQuote.attributes["statecode"];
                var objStatus = retrievedQuote.attributes["statuscode"];

                if (objState != undefined && objState != null && objState.value != null && objStatus != undefined && objStatus != null && objStatus.value != null) {
                    if (objState.value == 0 && objStatus.value == 1) {
                        result = true;
                    }
                }
            }
        }
    }
    return result;
}

function preFilterLookup() {
    Xrm.Page.getControl("pnl_machineid").addPreSearch(function () {
        addLookupFilter();
    });
}
function addLookupFilter() {
    var quote = Xrm.Page.getAttribute("pnl_quoteid").getValue();


    if (quote != null) {
        var cols = ["customerid"];
        var retrievedCustomer = XrmServiceToolkit.Soap.Retrieve("quote", quote[0].id, cols);
        if (retrievedCustomer.attributes.customerid != undefined && retrievedCustomer.attributes.customerid != null) {
            fetchXml = "<filter type='and'><condition attribute='pnl_currentcustomerid' operator='eq' value='" + retrievedCustomer.attributes.customerid.id + "' /></filter>";
            Xrm.Page.getControl("pnl_machineid").addCustomFilter(fetchXml);
        }
    }
}

function loadMachineValues() {
    var machine = Xrm.Page.getAttribute('pnl_machineid').getValue();
    if (machine != null) {
        var cols = ["pnl_approvedbookvalue", "pnl_marketvalue", "pnl_sellingmargin", "pnl_repaircosts", "pnl_sellingmarginpercentage", "pnl_appraisalstatus"];
        var retrievedMachine = XrmServiceToolkit.Soap.Retrieve("pnl_machine", machine[0].id, cols);
        if (retrievedMachine.attributes.pnl_bookvalue != undefined && retrievedMachine.attributes.pnl_bookvalue != null) {
            Xrm.Page.getAttribute('pnl_scenario1value').setValue(retrievedMachine.attributes.pnl_bookvalue.value);
        }
        if (retrievedMachine.attributes.pnl_bookvalue != undefined && retrievedMachine.attributes.pnl_bookvalue != null) {
            Xrm.Page.getAttribute('pnl_scenario2value').setValue(retrievedMachine.attributes.pnl_bookvalue.value);
        }
        if (retrievedMachine.attributes.pnl_marketvalue != undefined && retrievedMachine.attributes.pnl_marketvalue != null) {
            Xrm.Page.getAttribute('pnl_scenario3value').setValue(retrievedMachine.attributes.pnl_marketvalue.value);
        }
        if (retrievedMachine.attributes.pnl_marketvalue != undefined && retrievedMachine.attributes.pnl_marketvalue != null) {
            Xrm.Page.getAttribute('pnl_marketprice').setValue(retrievedMachine.attributes.pnl_marketvalue.value);
        }
        if (retrievedMachine.attributes.pnl_sellingmargin != undefined && retrievedMachine.attributes.pnl_sellingmargin != null) {
            Xrm.Page.getAttribute('pnl_sellingmargin').setValue(retrievedMachine.attributes.pnl_sellingmargin.value);
        }
        if (retrievedMachine.attributes.pnl_repaircosts != undefined && retrievedMachine.attributes.pnl_repaircosts != null) {
            Xrm.Page.getAttribute('pnl_repaircost').setValue(retrievedMachine.attributes.pnl_repaircosts.value);
        }
        if (retrievedMachine.attributes.pnl_sellingmarginpercentage != undefined && retrievedMachine.attributes.pnl_sellingmarginpercentage != null) {
            Xrm.Page.getAttribute('pnl_sellingmarginpercentage').setValue(retrievedMachine.attributes.pnl_sellingmarginpercentage.value);
        }
        if (retrievedMachine.attributes.pnl_appraisalstatus != undefined && retrievedMachine.attributes.pnl_appraisalstatus != null) {
            Xrm.Page.getAttribute('pnl_appraisalstatus').setValue(retrievedMachine.attributes.pnl_appraisalstatus.value);
        }
    }

}

function preFilterLookup_config() {
    Xrm.Page.getControl("pnl_configurationid").addPreSearch(function () {
        addLookupFilter_config();
    });
}
function addLookupFilter_config() {
    var quote = Xrm.Page.getAttribute("pnl_quoteid").getValue();
    if (quote != null) {
        fetchXml = "<filter type='and'><condition attribute='pnl_quoteid' operator='eq' value='" + quote[0].id + "' /></filter>";
        Xrm.Page.getControl("pnl_configurationid").addCustomFilter(fetchXml);
    }
}
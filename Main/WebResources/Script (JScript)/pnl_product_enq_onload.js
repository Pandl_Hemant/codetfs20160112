﻿function removeFieldFade_OnLoad() {
    SetFieldFades();
}

function removeFieldFade_OnSave() {
    window.setTimeout(SetFieldFades, 1000);
}

function removeFieldFade_OnChange() {
    window.setTimeout(SetFieldFades, 1000);
}

function SetFieldFades() {
    SetStyle("pnl_newusedid",true);
    SetStyle("pnl_stockclassmasterid",true);
    SetStyle("pnl_stockgroup",true);
    SetStyle("pnl_prontomodelid",true);
    SetStyle("pnl_usedmodelcode",false);
}

function SetStyle(editName,lookup) {

    var thisEdit = document.getElementById(editName);

    if (thisEdit == null) {
        throw "Failed to locate control: " + editName;
    }

    var nodes = thisEdit.getElementsByTagName('div');
    if (nodes != undefined && nodes != null && nodes.length > 0 && lookup == false) {
        RemoveFieldFade(nodes);
    }
    else {
        if (nodes != undefined && nodes != null && nodes.length > 0 && lookup) {
            RemoveLookupFade(nodes[0]);
        }
    }

}

function RemoveFieldFade(divnodes) {
    for (var i = 0; i < divnodes.length; i++) {
        if (divnodes[i].className != undefined && divnodes[i].className != null && divnodes[i].className == "ms-crm-Inline-GradientMask") {
            divnodes[i].setAttribute("class", "");
        }
    }
}

function RemoveLookupFade(divnodes) {
    var spans = divnodes.getElementsByTagName('span');
    for (var i = 0; i < spans.length; i++) {
        var divs = spans[i].getElementsByTagName('div');
        for (var j = 0; j < divs.length; j++) {
            if (divs[j].className != undefined && divs[j].className != null && divs[j].className == "ms-crm-Inline-GradientMask") {
                divs[j].setAttribute("class", "");
            }
        }
    }
}
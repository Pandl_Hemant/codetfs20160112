﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppraisalImageresize
{
    using System;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;

    using Microsoft.Xrm.Client;
    using Microsoft.Xrm.Client.Services;
    using Microsoft.Xrm.Sdk;
    using Microsoft.Xrm.Sdk.Query;

    public class Appraisalimageresize : IPlugin
	{

        public void Execute(IServiceProvider serviceProvider)
        {
            Microsoft.Xrm.Sdk.IPluginExecutionContext context = (Microsoft.Xrm.Sdk.IPluginExecutionContext)
            serviceProvider.GetService(typeof(Microsoft.Xrm.Sdk.IPluginExecutionContext));

            if (context.Depth == 1 || context.Depth == 3)
            {
                IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService _organizationService = serviceFactory.CreateOrganizationService(context.UserId);
                Entity entity = (Entity)context.InputParameters["Target"];
                Entity posEventService = (Entity)context.PostEntityImages["entity"];
                if (posEventService.Attributes.Contains("documentbody"))
                {
                    string filename = (String)posEventService.Attributes["filename"];
                    if (filename.StartsWith("Photo-"))
                    {
                        var maxWidth = 1024;
                        var maxHeight = 768;
                        EntityReference appPhoto = (EntityReference)(posEventService["objectid"]);
                        var imageBytes = Convert.FromBase64String(posEventService["documentbody"].ToString());
                        var start = DateTime.Now;
                        var image = ConvertBytesToImage(imageBytes);
                        var scaledImage = image.Scale(new Size(maxWidth, maxHeight));
                        var jgpEncoder = GetEncoder(ImageFormat.Jpeg);
                        var myEncoder = Encoder.Quality;
                        var myEncoderParameters = new EncoderParameters(1);
                        var myEncoderParameter = new EncoderParameter(myEncoder, 100L);
                        myEncoderParameters.Param[0] = myEncoderParameter;
                        // scaledImage.Save("C:\\Users\\Simon.Kendall\\Downloads\\LandPower\\AppraisalPhotos\\ScaledPhoto.jpg", jgpEncoder, myEncoderParameters);
                        byte[] data;
                        using (var m = new MemoryStream())
                        {
                            scaledImage.Save(m, jgpEncoder, myEncoderParameters);
                            data = m.ToArray();
                        }

                        var note = new Entity("annotation");
                        note["subject"] = "Photo resized";
                        note["filename"] = "photoresized-"+posEventService.Id+".jpg";
                        note["documentbody"] = Convert.ToBase64String(data);
                        note["mimetype"] = "image/jpg";
                        note["objectid"] = new EntityReference("pnl_appraisalphoto", appPhoto.Id);
                        _organizationService.Create(note);

                    }
                }
            }
        }

        private static ImageCodecInfo GetEncoder(ImageFormat format)
        {

            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();

            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }

        internal static Image ConvertBytesToImage(byte[] byteArrayIn)
        {
            using (var ms = new MemoryStream(byteArrayIn))
            {
                return Image.FromStream(ms);
            }
        }

        //internal static byte[] GetImageContentFromCrm()
        //{
        //    // Note ids for Praveena TestF Appraisal - D0A9844E-1FA8-E411-80F5-0050568A79C5, D7824A3F-1FA8-E411-80F5-0050568A79C5, 56377A47-1FA8-E411-80F5-0050568A79C5, 
        //    //	D6824A3F-1FA8-E411-80F5-0050568A79C5, 01B06556-1FA8-E411-80F5-0050568A79C5.
        //    // Note ids for Praveena monv Appraisal - 77A07983-4BAA-E411-80F5-0050568A79C5, 29E7448E-4BAA-E411-80F5-0050568A79C5, 28E7448E-4BAA-E411-80F5-0050568A79C5, 
        //    //	9B02D19C-4BAA-E411-80F5-0050568A79C5, 79A07983-4BAA-E411-80F5-0050568A79C5, 754E4796-4BAA-E411-80F5-0050568A79C5, 764E4796-4BAA-E411-80F5-0050568A79C5, 
        //    //	2AE7448E-4BAA-E411-80F5-0050568A79C5, 744E4796-4BAA-E411-80F5-0050568A79C5, 78A07983-4BAA-E411-80F5-0050568A79C5
        //    //var noteId = new Guid("6FD1A67E-2E70-E411-80F4-0050568A79C5"); // .png, 2048 x 1536
        //    //var noteId = new Guid("917FBB03-F353-E411-80F4-0050568A79C5"); // .jpg
        //    var noteId = new Guid("78A07983-4BAA-E411-80F5-0050568A79C5");
        //    var entity = _organizationService.Retrieve("annotation", noteId, new ColumnSet(true));

        //    return Convert.FromBase64String(entity["documentbody"].ToString());
        //}

        //internal static IOrganizationService GetOrganizationService()
        //{
        //    var connectionString = _crmConnectionString;
        //    var crmConnection = CrmConnection.Parse(connectionString);

        //    return new OrganizationService(crmConnection);
        //}
    }
}
//string fetchAccountType = @"<fetch mapping='logical'>" + 
//                             "<entity name='annotation'>    " + 
//                             "<attribute name='filename' />" + 
//                             "<attribute name='annotationid' />    " + 
//                             "<filter type='and'>" + 
//                             "<condition attribute='filename' operator='like' value='Photo-%' />" + 
//                             "</filter>" + 
//                             "<link-entity name='pnl_appraisalphoto' from='pnl_appraisalphotoid' to='objectid' alias='ab'>" + 
//                             "<filter type='and'>" + 
//                             "<condition attribute='pnl_appraisalid' operator='not-null' />" + 
//                             "</filter>" + 
//                             "</link-entity>" + 
//                             "</entity>" + 
//                             "</fetch>";
//var entityCollection = _organizationService.RetrieveMultiple(new FetchExpression(fetchAccountType));


//var finish = DateTime.Now;
//Console.WriteLine("Scaled image in {0} milliseconds.", (finish - start).Milliseconds); // 140ms

// Image(Bitmap) defaults to .png. Turn it back into a .jpg to minimize the file size.
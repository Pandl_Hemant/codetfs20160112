﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using Moq;

namespace LandPower.BusinessLogic.Tests
{
	using Data;

	[TestClass]
	public class AppraisalItemTests
	{
        [TestMethod]
        public void ExecutePaulsAppraisalItemWorkflow()
        {
            var workflowId = new Guid("479125C6-42FC-4B89-BFEB-7436947F1A34");
            var organizationService = GetOrganizationService();
            var context = new CrmServiceContext(organizationService);
            var appraisalItems = (from a in context.pnl_appraisalitemSet where a.statecode == pnl_appraisalitemState.Active select a).ToList();

            //for (var i = 0; i < 100; i++ )
            foreach(var appaisalItem in appraisalItems)
            {
                var request = new ExecuteWorkflowRequest
                {
                    WorkflowId = workflowId,
                    //EntityId = appraisalItems[i].Id
                    EntityId = appaisalItem.Id
                };
                organizationService.Execute(request);
            }
        }

        internal IOrganizationService GetOrganizationService()
        {
            const string connectionString = "Url=https://idealdev.landpower.co.nz; Username=hemantk@landpower; Password=Pass@word;";
            var crmConnection = CrmConnection.Parse(connectionString);
            return new OrganizationService(crmConnection);
        }

		[TestMethod]
		public void GetItemValueNullTest()
		{
			var target = new pnl_appraisalitem();

			var actual = target.GetItemValue();

			Assert.AreEqual(0m, actual);
		}

		[TestMethod]
		public void GetItemValuePicklistTest()
		{
			var target = new pnl_appraisalitem {pnl_FieldType = new OptionSetValue((int) pnl_appraisalitemfieldtype.Picklist)};

			var actual = target.GetItemValue();

			Assert.AreEqual(0m, actual);
		}

		[TestMethod]
		public void GetItemValueMemoTest()
		{
			var target = new pnl_appraisalitem { pnl_FieldType = new OptionSetValue((int)pnl_appraisalitemfieldtype.Memo) };

			var actual = target.GetItemValue();

			Assert.AreEqual(0m, actual);
		}

		[TestMethod]
		public void GetItemValueStringTest()
		{
			var target = new pnl_appraisalitem
			{
				pnl_FieldType = new OptionSetValue((int)pnl_appraisalitemfieldtype.String),
				pnl_Value = "Test"
			};

			var actual = target.GetItemValue();

			Assert.AreEqual(0m, actual);
		}

		[TestMethod]
		public void GetItemValueIntegerTest()
		{
			var target = new pnl_appraisalitem
			{
				pnl_FieldType = new OptionSetValue((int)pnl_appraisalitemfieldtype.Integer),
				pnl_Value = "12458"
			};

			var actual = target.GetItemValue();

			Assert.AreEqual(12458m, actual);
		}

		[TestMethod]
		public void GetItemValueIntegerNullTest()
		{
			var target = new pnl_appraisalitem
			{
				pnl_FieldType = new OptionSetValue((int)pnl_appraisalitemfieldtype.Integer)
			};

			var actual = target.GetItemValue();

			Assert.AreEqual(0m, actual);
		}

		[TestMethod]
		public void GetAverageValuationZeroTest()
		{
			var target = new pnl_appraisal();
			var mockService = new Mock<IOrganizationService> { CallBase = false };
			var mockContext = GetMockServiceContext(mockService.Object);
			var mockAppraisalExtensions = new Mock<IAppraisalExtensions> { CallBase = true };
			mockAppraisalExtensions.Setup(a => a.FetchAppraisalValuationItems(It.IsAny<Guid>()))
				.Returns(new List<pnl_appraisalitem>());

			AppraisalExtension.AppraisalFactory = context => mockAppraisalExtensions.Object; // Replace the factory with one that returns the mock.

			var actual = target.GetAverageValuation(mockContext.Object);

			Assert.AreEqual(0m, actual);
		}

		[TestMethod]
		public void GetAverageValuationRoundedDownTest()
		{
			var target = new pnl_appraisal();
			var mockService = new Mock<IOrganizationService> { CallBase = false };
			var mockContext = GetMockServiceContext(mockService.Object);
			var mockAppraisalExtensions = new Mock<IAppraisalExtensions> { CallBase = true };
			mockAppraisalExtensions.Setup(a => a.FetchAppraisalValuationItems(It.IsAny<Guid>()))
				.Returns(new List<pnl_appraisalitem> { new pnl_appraisalitem{ pnl_Value = "$24,459.24" } });

			AppraisalExtension.AppraisalFactory = context => mockAppraisalExtensions.Object; // Replace the factory with one that returns the mock.

			var actual = target.GetAverageValuation(mockContext.Object);

			Assert.AreEqual(24459m, actual);
		}

		[TestMethod]
		public void GetAverageValuationRoundedUpTest()
		{
			var target = new pnl_appraisal();
			var mockService = new Mock<IOrganizationService> { CallBase = false };
			var mockContext = GetMockServiceContext(mockService.Object);
			var mockAppraisalExtensions = new Mock<IAppraisalExtensions> { CallBase = true };
			mockAppraisalExtensions.Setup(a => a.FetchAppraisalValuationItems(It.IsAny<Guid>()))
				.Returns(new List<pnl_appraisalitem> { new pnl_appraisalitem { pnl_Value = "$24,459.50" } });

			AppraisalExtension.AppraisalFactory = context => mockAppraisalExtensions.Object; // Replace the factory with one that returns the mock.

			var actual = target.GetAverageValuation(mockContext.Object);

			Assert.AreEqual(24460m, actual);
		}

		internal Mock<CrmServiceContext> GetMockServiceContext(IOrganizationService organizationService)
		{
			var mockContext = new Mock<CrmServiceContext>(organizationService);
			//mockContext.Setup(c => c.pnl_delegatedfinancialauthoritySet).Returns(GetFakeDelegatedFinancialAuthorities);

			return mockContext;
		}
	}
}

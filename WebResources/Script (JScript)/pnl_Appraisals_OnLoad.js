function onLoad() {

    // removes all field fades on labels
    load_css_file('/WebResources/pnl_FormRemoveLabelFade');

    removeFieldFade_OnLoad();

    SetWorksheetStyle();
}

function load_css_file(filename) {
    var fileref = document.createElement("link");
    fileref.setAttribute("rel", "stylesheet");
    fileref.setAttribute("type", "text/css");
    fileref.setAttribute("href", filename);
    document.getElementsByTagName("head")[0].appendChild(fileref);
}

function removeFieldFade_OnLoad() {
    var attributes = Xrm.Page.data.entity.attributes.get();
    for (var i in attributes) {
        var thisEdit = document.getElementById(attributes[i].getName());
        if (thisEdit != undefined && thisEdit != null) {
            var nodes = thisEdit.getElementsByTagName('div');
            if (nodes != undefined && nodes != null) {
                for (var j = 0; j < nodes.length; j++) {
                    if (nodes[j].className != undefined && nodes[j].className != null && nodes[j].className == "ms-crm-Inline-GradientMask") {
                        nodes[j].setAttribute("class", "");
                    }
                }
            }
        }
    }
}

function RemoveFadeOnCurrency(divnodes) {
    for (var i = 0; i < divnodes.length; i++) {
        if (divnodes[i].className != undefined && divnodes[i].className != null && divnodes[i].className == "ms-crm-Inline-GradientMask") {
            divnodes[i].setAttribute("class", "");
        }
    }
}

function SetWorksheetStyle() {

    SetNonApproverControlsToReadOnly();

   if (CanApproveTradePrice()) {
        var status = Xrm.Page.getAttribute("statuscode").getValue();
        if (status == 125760000 || status == 1) {
            SetReadOnly("pnl_approvedtradeprice",false);
        }
    }
    // sets editable currency to right-aligned non-fade black text
    SetCurrencyEditStyle("pnl_calculatedaverage", true);
    SetCurrencyEditStyle("pnl_recommededlistprice", true);

    SetCurrencyEditStyle("pnl_repaircosts", true);
    SetCurrencyEditStyle("pnl_freight", true);
    SetCurrencyEditStyle("pnl_predeliveryexpenses", true);
    SetCurrencyEditStyle("pnl_othercosts", true);
    SetCurrencyEditStyle("pnl_totalcosts", true);

    SetCurrencyEditStyle("pnl_expectedretainedmargindollar", true);

    SetCurrencyEditStyle("pnl_calcuatedtradeprice", true);
    SetCurrencyEditStyle("pnl_recommendedtradeprice", true);
    SetCurrencyEditStyle("pnl_approvedtradeprice", true);
    SetCurrencyEditStyle("pnl_approvedtradeprice1", true);

    // sets label to hide Lock image
    SetLabelStyle("modifiedon", false);
    SetLabelStyle("pnl_approvalrequestedon", false);
    SetLabelStyle("pnl_valuationapprovedon", false);
    SetLabelStyle("pnl_valuationapprovedby", false);
    //SetLabelStyle("pnl_approvedtradeprice", false);
    //SetLabelStyle("pnl_approvedtradeprice1", false);


    var thisEdit = document.getElementById("pnl_approvedtradeprice1");
    var nodes = thisEdit.getElementsByTagName('div');
    RemoveFadeOnCurrency(nodes)


    WaitHeaderFieldFade();

    // checks that all required appraisal items are completed
    if (GetUncompletedAppraisalItems()) {
        Xrm.Page.ui.setFormNotification("This Appraisal contains Items marked as Required that have not been assigned a value.");
    }
}

function CanApproveTradePrice() {

    var approveRolesAndTeamRoles = "System Administrator, Appraisal Approver";
    var userRoles = GetAllUserRolesAndTeamRoles();

    var canApprove = false;

    for (var i = 0; i < userRoles.length; i++) {
        var roleName = userRoles[i];
        if (roleName != null && roleName != "") {
            if (approveRolesAndTeamRoles.indexOf(roleName) > -1) {
                canApprove = true;
                break;
            }
        }
    }

    return canApprove;
}

function AllowedToChangeForm() {
    var allowed = true;

    // set form readonly if waiting for approval or approved
    var status = Xrm.Page.getAttribute("statuscode").getValue();

    if (status == 125760000 || status == 125760001) {
        allowed = false;
    }
    return allowed;
}

function SetNonApproverControlsToReadOnly() {
    MakeSubGridsReadOnly();

    SetReadOnly("pnl_name", true);
    SetReadOnly("pnl_appraisaltemplateid", true);
    SetReadOnly("pnl_appraisaldate", true);
    SetReadOnly("pnl_machineid", true);


    SetReadOnly("pnl_calculatedaverage", true);
    SetReadOnly("pnl_recommededlistprice", true);

    SetReadOnly("pnl_repaircosts", true);
    SetReadOnly("pnl_freight", true);
    SetReadOnly("pnl_predeliveryexpenses", true);
    SetReadOnly("pnl_othercosts", true);
    SetReadOnly("pnl_totalcosts", true);

    SetReadOnly("pnl_expectedretainedmarginpercentage", true);
    SetReadOnly("pnl_expectedretainedmargindollar", true);

    SetReadOnly("pnl_calcuatedtradeprice", true);
    SetReadOnly("pnl_recommendedtradeprice", true);
    SetReadOnly("pnl_approvedtradeprice", true);
    SetReadOnly("pnl_approvedtradeprice1", true);

    SetReadOnly("pnl_primaryapprover", true);
    SetReadOnly("pnl_valuationapprovedon", true);
    SetReadOnly("pnl_valuationapprovedby", true);
    SetReadOnly("pnl_approvalrequestedon", true);

}

function MakeSubGridsReadOnly() {
    // removes buttons from subGrid
    var intervalId;
    try {
        var subgridsLoaded = false;
        Xrm.Page.ui.controls.get().forEach(
			function (control, index) {
			    if (control.setDisabled && Xrm.Page.ui.getFormType() == 3) {
			        RemoveButtonsFromSubGrid(control, intervalId);
			        subgridsLoaded = true;
			    }
			}
		);

        if ($("div[id$='_crmGridTD']").length > 0 && !subgridsLoaded) {
            intervalId = setInterval(function () {
                var subgridsArr = Xrm.Page.getControl(function (control, index) {
                    return control.getControlType() == 'subgrid';
                });
                subgridsArr.forEach(function (control, index) {
                    RemoveButtonsFromSubGrid(control, intervalId);
                });
            }, 500);
        }
    }
    catch (e) {
        alert("makeSubGridsReadOnly() Error: " + e.message);
    }
}

function RemoveButtonsFromSubGrid(subgridControl, intervalId) {
    if (intervalId) {
        $('#' + subgridControl.getName() + '_addImageButton').css('display', 'none');
        $('#' + subgridControl.getName() + '_openAssociatedGridViewImageButton').css('display', 'none');
        clearInterval(intervalId);
    }
}

function SetReadOnly(editName, isReadOnly) {
    var thisEdit = Xrm.Page.getControl(editName);
    if (!thisEdit) { return; }
    if (thisEdit.getDisabled() == isReadOnly)
        return;
    thisEdit.setDisabled(isReadOnly);

    var labelEdit = document.getElementById(editName);
    if (labelEdit) {
        HideLockIcon(labelEdit);
    }
}

function WaitHeaderFieldFade() {
    window.setTimeout(HeaderFieldFade, 10);
}

function HeaderFieldFade() {
    var headermain = document.getElementById("HeaderTilesWrapperLayoutElement");
    var headerclass = headermain.getElementsByClassName("ms-crm-HeaderTilesWrapperElement");
    var header1 = headerclass[0].getElementsByClassName("ms-crm-HeaderTileElement ms-crm-HeaderTileElementPosition0");
    var header2 = headerclass[0].getElementsByClassName("ms-crm-HeaderTileElement ms-crm-HeaderTileElementPosition1");
    header1[0].style.width = '250px';
    header2[0].style.width = '250px';
}

function SetLabelStyle(labelName, isRightAligned) {

    var labelEdit = document.getElementById(labelName);

    if (labelEdit == null) {
        throw "Failed to locate control: " + labelName;
    }

    var nodes = labelEdit.getElementsByTagName('div');
    if (nodes != undefined && nodes != null && nodes.length > 0) {
        if (isRightAligned) {
            nodes[0].style.textAlign = 'right';
        }
    }

    HideLockIcon(labelEdit);

    labelEdit.style.color = "black";
}

function SetCurrencyEditStyle(editName) {

    var thisEdit = document.getElementById(editName);

    if (thisEdit == null) {
        throw "Failed to locate control: " + editName;
    }

    var nodes = thisEdit.getElementsByTagName('div');
    if (nodes != undefined && nodes != null && nodes.length > 0) {
        nodes[0].style.textAlign = 'right';
        //RemoveFadeOnCurrency(nodes);
    }

    HideLockIcon(thisEdit);

    thisEdit.style.textAlign = 'right';
    thisEdit.style.color = "black";

    if (Xrm.Page.getControl(editName).getDisabled() == false) {
        thisEdit.style.borderBottomColor = 'yellow';
        thisEdit.style.backgroundColor = 'yellow';

        var currencySymbolContainer = document.getElementById(editName + "_sym");
        if (currencySymbolContainer != null) {
            currencySymbolContainer.style.borderBottomColor = 'yellow';
            currencySymbolContainer.style.backgroundColor = 'yellow';
        }
    }
    else {
        thisEdit.style.borderBottomColor = '';
        thisEdit.style.backgroundColor = '';
    }

    //thisEdit.style.display = 'none';
    //var redrawFix = thisEdit.offsetHeight;
    thisEdit.style.display = 'block';
}

function HideLockIcon(editObject) {
    var imgnodes = editObject.getElementsByTagName('img');
    if (imgnodes != undefined && imgnodes != null && imgnodes.length > 0) {
        for (var i = 0; i < imgnodes.length; i++) {
            if (imgnodes[i].className != undefined && imgnodes[i].className != null && imgnodes[i].className == "ms-crm-ImageStrip-inlineedit_locked") {
                imgnodes[i].setAttribute("class", "");
            }
        }
    }
}

function GetUncompletedAppraisalItems() {
    ///<summary>
    /// Sends synchronous requests to retrieve the appraisal items list, checks to see if any are required. Returns true if any required do not have values.
    ///</summary>

    // gets Id associated with the Appraisal Items
    var appraisalId = Xrm.Page.data.entity.getId();

    var querystart = "<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>" +
                    "<entity name='pnl_appraisalitem'>" +
                    "<attribute name='pnl_name' />" +
                    "<attribute name='pnl_value' />" +
                    "<attribute name='pnl_required' />" +
                    "<attribute name='pnl_appraisalitempicklistid' />" +
                    "<order attribute='pnl_name' descending='false' />" +
					"<filter type='and'>" +
                          "<condition attribute='pnl_appraisalid' operator='eq' value='" + appraisalId + "' />" +
                          "<condition attribute='pnl_required' operator='eq' value='1' />" +
                        "</filter>";

    var queryend = "</entity></fetch>";

    var picklists =
					"<link-entity name='pnl_appraisalitempicklist' from='pnl_appraisalitempicklistid' to='pnl_appraisalitempicklistid' alias='aa'>" +
						"<attribute name='pnl_name' alias='picklistname' />" +
                      "</link-entity>";

    var nonpicklists = "<filter type='and'>" +
                          "<condition attribute='pnl_appraisalitempicklistid' operator='null' />" +
                        "</filter>";

    // fetch any required picklist appraisal items
    var xml = [querystart + picklists + queryend].join("");
    var fetchResult1 = XrmServiceToolkit.Soap.Fetch(xml);

    // fetch any other required appraisal items
    var xml2 = [querystart + nonpicklists + queryend].join("");
    var fetchResult2 = XrmServiceToolkit.Soap.Fetch(xml2);

    var fetchResult = fetchResult1.concat(fetchResult2);

    if (fetchResult !== null && typeof fetchResult != 'undefined') {
        for (var i = 0; i < fetchResult.length; i++) {

            // a picklist
            if (fetchResult[i].attributes["pnl_appraisalitempicklistid"]) {
                if (!fetchResult[i].attributes["pnl_value"]) {
                    // picklist does not have a value set!
                    return true;
                }
                // not a picklist
            } else {
                if (!fetchResult[i].attributes["pnl_value"]) {
                    // field does not have a value set!
                    return true;
                } else if (fetchResult[i].attributes["pnl_value"].value == "") {
                    // field has an empty value set!
                    return true;
                }
            }
        }
    }

    return false;  // required appraisal items have all values set
}

//Executed from the Request Approval Ribbon Button
function RequestApproval() {
    try {
        Xrm.Page.ui.setFormNotification("Requesting Approval, Please Wait...", "WARNING", "3");
        window.setTimeout(RequestApprovalExecute, 10);
    }
    catch (err) {
        alert(err.message);
        Xrm.Page.ui.clearFormNotification("3");
    }
    HeaderFieldFade();
}

function RequestApprovalExecute() {
    var currentDateTime = new Date();
    var requestDate = Xrm.Page.getAttribute("pnl_approvalrequestedon");
    requestDate.setSubmitMode("always");
    requestDate.setValue(currentDateTime); // sets Requested Approval On date

    Xrm.Page.data.save().then(
        function () {
            //saStatus.setSubmitMode("dirty");
            //if (XrmServiceToolkit.Soap.SetState("pnl_appraisal", Xrm.Page.data.entity.getId(), 0, 125760000)) {
                window.setTimeout(forceHardPageRefresh, 10);
            //}
            Xrm.Page.ui.clearFormNotification("3");
        },
        function () {
            //alert("Save Fail");
            //saStatus.setSubmitMode("dirty");
            //Xrm.Page.ui.clearFormNotification("3");
        });
    return;
}

//Executed from the Approve Ribbon Button
function ApproveAppraisal() {
    try {
        Xrm.Page.ui.setFormNotification("Approval Processing, Please Wait...", "WARNING", "3");
        window.setTimeout(ApproveAppraisalExecute, 10);
    }
    catch (err) {
        alert(err.message);
        Xrm.Page.ui.clearFormNotification("3");
    }
    HeaderFieldFade();
}

function ApproveAppraisalExecute() {
    var currentDateTime = new Date();
    var requestDate = Xrm.Page.getAttribute("pnl_valuationapprovedon");
    requestDate.setSubmitMode("always");
    requestDate.setValue(currentDateTime); // sets  Approved On

    Xrm.Page.data.save().then(
        function () {
            //if (XrmServiceToolkit.Soap.SetState("pnl_appraisal", Xrm.Page.data.entity.getId(), 0, 125760001)) {
                window.setTimeout(forceHardPageRefresh, 10);
            //}
            Xrm.Page.ui.clearFormNotification("3");
        },
        function () {
            //alert("Save Fail");
            //saStatus.setSubmitMode("dirty");
            //Xrm.Page.ui.clearFormNotification("3");
        });
    return;
}

function forceHardPageRefresh() {
    Mscrm.ReadFormUtilities.openInSameFrame(window._etc, Xrm.Page.data.entity.getId());
}

var resizeTimer;
$(window).resize(function () {
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(HeaderFieldFade, 10);
});

function GetAllUserRolesAndTeamRoles() {
    var guid = "[A-z0-9]{8}-[A-z0-9]{4}-[A-z0-9]{4}-[A-z0-9]{4}-[A-z0-9]{12}";

    var serverUrl = Xrm.Page.context.getClientUrl();
    var userId = Xrm.Page.context.getUserId();
    userId = userId.match(guid);

    var teamQuery = "TeamMembershipSet?$select=TeamId&$filter=SystemUserId eq guid'" + userId + "'";
    var teamRoleQuery = "TeamRolesSet?$select=RoleId&$filter=";
    var roleQuery = "RoleSet?$select=Name&$filter=";

    var teams = MakeRequest(serverUrl, teamQuery, 0);

    teamRoleQuery = ComposeQuery(teamRoleQuery, "TeamId", teams);
    var teamRoles = MakeRequest(serverUrl, teamRoleQuery, 1);

    var userRoles = Xrm.Page.context.getUserRoles();

    if (userRoles != null) {
        for (var i = 0; i < userRoles.length; i++) {
            teamRoles.push(userRoles[i].match(guid));
        }
    }

    roleQuery = ComposeQuery(roleQuery, "RoleId", teamRoles);
    var roles = MakeRequest(serverUrl, roleQuery, 2);

    return roles;
}

function MakeRequest(serverUrl, query, type) {

    var oDataEndpointUrl = serverUrl + "/XRMServices/2011/OrganizationData.svc/";
    oDataEndpointUrl += query;

    var service = GetRequestObject();

    if (service != null) {

        service.open("GET", oDataEndpointUrl, false);
        service.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        service.setRequestHeader("Accept", "application/json, text/javascript, */*");
        service.send(null);

        var retrieved = $.parseJSON(service.responseText).d;

        var results = new Array();

        switch (type) {

            case 0:
                for (var i = 0; i < retrieved.results.length; i++) {
                    results.push(retrieved.results[i].TeamId);
                }
                break;

            case 1:
                for (var i = 0; i < retrieved.results.length; i++) {
                    results.push(retrieved.results[i].RoleId);
                }
                break;

            case 2:
                for (var i = 0; i < retrieved.results.length; i++) {
                    if (results.indexOf(retrieved.results[i].Name) == -1) {
                        results.push(retrieved.results[i].Name);
                    }
                }
                break;
        }
        return results;
    }
    return null;
}

function GetRequestObject() {

    if (window.XMLHttpRequest) {
        return new window.XMLHttpRequest;
    }
    else {
        try {
            return new ActiveXObject("MSXML2.XMLHTTP.3.0");
        }
        catch (ex) {
            return null;
        }
    }
}

function ComposeQuery(queryBase, attribute, items) {
    if (items != null) {
        for (var i = 0; i < items.length; i++) {
            if (i == 0) {
                queryBase += attribute + " eq (guid'" + items[i] + "')";
            }
            else {
                queryBase += " or " + attribute + " eq (guid'" + items[i] + "')";
            }
        }
    }
    return queryBase;
}

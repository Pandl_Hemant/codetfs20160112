SELECT bu.pnl_defaultvalueofincludegstinquotereport AS bu_pnl_defaultvalueofincludegstinquotereport
FROM FilteredSystemUser systemuser
LEFT OUTER JOIN FilteredBusinessUnit bu ON systemuser.businessunitid = bu.businessunitid
WHERE systemuser.systemuserid = dbo.fn_FindUserGuid()

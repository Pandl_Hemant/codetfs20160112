DECLARE @pQuote uniqueidentifier; SET @pQuote = '678F78E1-884B-E511-80FA-0050568A79C5';
SELECT
	IsNull(pnl_SuperCatDesc, 
		CASE WHEN quotedetail.pnl_islocallysourced = 1 OR quotedetail.pnl_locallysourceditemid IS NOT NULL THEN 'Locally Sourced' ELSE
		(SELECT TOP 1 d2.productdescription 
		FROM FilteredQuoteDetail d2 
		WHERE d2.quoteid=quotedetail.quoteid AND d2.pnl_spectypename='Section' 
		AND d2.pnl_specs_sales_seq_no<quotedetail.pnl_specs_sales_seq_no 
		AND d2.exp_linenumber=quotedetail.exp_linenumber
		ORDER BY d2.pnl_specs_sales_seq_no DESC) END) 
		AS pnl_SuperCatDesc,
		pnl_SuperCatDesc AS pnl_SuperCatDesc_REAL,
	pnl_specs_sales_seq_no,
	quotedetail.quoteid, 
	quotedetail.productid, 
	quotedetail.productdescription, 
	quotedetail.priceperunit, 
	quotedetail.quantity, 
	quotedetail.extendedamount, 
	quotedetail.quotedetailid, 
	quotedetail.lineitemnumber, 
	quotedetail.new_expudf3, 
	quotedetail.new_expudf2, 
	quotedetail.new_expudf1, 
	quotedetail.exp_linenumber, 
	quotedetail.pnl_spectypename, 
	quotedetail.pnl_islocallysourcedname, 
	quotedetail.pnl_istradein, 
	quotedetail.pnl_basepriceandselectedspecs,
	locallysourceditem.pnl_name AS locallysourceditem_pnl_name, 
	locallysourceditem.pnl_confignumber AS locallysourceditem_pnl_confignumber, 
	locallysourceditem.pnl_linenumber AS locallysourceditem_pnl_linenumber, 
	locallysourceditem.pnl_listprice AS locallysourceditem_pnl_listprice,
	quote.name AS quote_name, 
	quote.pnl_landpowerquoteid AS quote_pnl_landpowerquoteid, 
	quote.pnl_highestvaluemachinename AS quote_pnl_highestvaluemachinename, 
	quote.customerid AS quote_customerid, 
	account.primarycontactid AS account_primarycontactid
FROM Filteredquotedetail AS quotedetail
LEFT OUTER JOIN Filteredpnl_locallysourceditem AS locallysourceditem ON quotedetail.pnl_locallysourceditemid = locallysourceditem.pnl_locallysourceditemid
LEFT OUTER JOIN FilteredQuote quote ON quotedetail.quoteid = quote.quoteid
LEFT OUTER JOIN FilteredAccount account ON quote.customerid = account.accountid
WHERE quotedetail.quoteid = @pQuote
AND (quotedetail.exp_linenumber IS NOT NULL OR quotedetail.pnl_locallysourceditemid IS NOT NULL)
AND (quotedetail.pnl_spectypename IS NULL OR quotedetail.pnl_spectypename <> 'Section')
ORDER BY IsNull(quotedetail.exp_linenumber, locallysourceditem.pnl_confignumber), IsNull(quotedetail.pnl_specs_sales_seq_no, 999999), quotedetail.extendedamount DESC
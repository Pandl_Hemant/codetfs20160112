DECLARE @ordernumber NVARCHAR(MAX);
SET @ordernumber = 'ORD-01579-S5G0';
/*
	Calculated fieldnames:-

	    ConfigNumber">
        LineNumber">
        ExtendedAmountValue">
        ExtendedAmountString">
*/
SELECT
	locallysourceditem.pnl_confignumber AS locallysourceditem_pnl_confignumber,
	locallysourceditem.pnl_discountpercentage AS locallysourceditem_pnl_discountpercentage,
	locallysourceditem.pnl_linenumber AS locallysourceditem_pnl_linenumber,
	locallysourceditem.pnl_listprice AS locallysourceditem_pnl_listprice, 
	locallysourceditem.pnl_locallysourceddnp AS locallysourceditem_pnl_locallysourceddnp,
	locallysourceditem.pnl_name AS locallysourceditem_pnl_name, 
	ord.ordernumber AS ord_ordernumber, 	
	ord.pnl_salesaidapproved AS ord2_pnl_salesaidapproved, 
	ord.pnl_totalotherdealercosts AS ord2_pnl_totalotherdealercosts, 	
	qotd.exp_linenumber AS qotd_exp_linenumber, 
	qotd.pnl_specs_sales_seq_no AS qotd_pnl_specs_sales_seq_no,
	salesorderdetail.exp_linenumber, 
	salesorderdetail.extendedamount, 
	salesorderdetail.pnl_basepriceandselectedspecs,
	salesorderdetail.pnl_bpass_dnpdollar,
	salesorderdetail.pnl_instkisdealerstock, 
	salesorderdetail.pnl_instklocation,  
	salesorderdetail.pnl_instkmachinestatusname,  
	salesorderdetail.pnl_instkprovisional_serial_no,  
	salesorderdetail.pnl_instkserial_no,  
	salesorderdetail.pnl_islocallysourced,  
	salesorderdetail.pnl_istradein,
	salesorderdetail.lineitemnumber,
	salesorderdetail.pnl_locallysourceddnp,
	salesorderdetail.pnl_machinecostsindentdiscount, 
	salesorderdetail.pnl_machinecostsmonthlyprogdollar, 
	salesorderdetail.pnl_machinecostsmonthlyprogpercent, 
	salesorderdetail.pnl_machinecostsstandarddiscount, 
	salesorderdetail.pnl_mod_sup_stock_code, 
	salesorderdetail.pnl_quoteproductlocallysourceditemname,
	salesorderdetail.pnl_spec_stock_code, 
	salesorderdetail.pnl_specs_sales_stock_code, 
	salesorderdetail.pnl_spectypename, 
	salesorderdetail.priceperunit * salesorderdetail.quantity as listPrice,
	salesorderdetail.priceperunit, 
	salesorderdetail.productdescription, 
	salesorderdetail.productid, 
	salesorderdetail.quantity
FROM FilteredSalesorderDetail AS salesorderdetail
INNER JOIN FilteredSalesOrder AS ord ON salesorderdetail.salesorderid = ord.salesorderid
LEFT OUTER JOIN FilteredQuoteDetail AS qotd ON (salesorderdetail.exp_recordid = qotd.exp_recordid AND salesorderdetail.exp_linenumber = qotd.exp_linenumber AND qotd.createdonbehalfby IS NOT NULL)
LEFT OUTER JOIN Filteredpnl_locallysourceditem AS locallysourceditem ON salesorderdetail.pnl_quoteproductlocallysourceditem = locallysourceditem.pnl_locallysourceditemid
WHERE ord.ordernumber = @ordernumber
ORDER BY salesorderdetail.exp_linenumber, salesorderdetail.lineitemnumber

/*
        <Field Name="ConfigNumber">
          <Value>=IIF(IsNothing(Fields!exp_linenumber.Value),Fields!locallysourceditem_pnl_confignumber.Value,Fields!exp_linenumber.Value)</Value>
        </Field>
        <Field Name="LineNumber">
          <Value>=IIF(IsNothing(Fields!lineitemnumber.Value),Fields!locallysourceditem_pnl_linenumber.Value,Fields!lineitemnumber.Value)</Value>
        </Field>
        <Field Name="productdescription">
          <DataField>productdescription</DataField>
          <rd:TypeName>System.String</rd:TypeName>
        </Field>
        <Field Name="ExtendedAmountValue">
          <Value>=IIF(IsNothing(Fields!exp_linenumber.Value),Fields!locallysourceditem_pnl_listprice.Value,Fields!extendedamount.Value)</Value>
        </Field>
        <Field Name="priceperunit">
          <DataField>priceperunit</DataField>
          <rd:TypeName>System.Decimal</rd:TypeName>
        </Field>
        <Field Name="qotd_pnl_specs_sales_seq_no">
          <DataField>qotd_pnl_specs_sales_seq_no</DataField>
          <rd:TypeName>System.Decimal</rd:TypeName>
        </Field>
        <Field Name="ExtendedAmountString">
          <Value>=IIF(IsNothing(Fields!exp_linenumber.Value),Fields!locallysourceditem_pnl_listprice.Value,Fields!extendedamount.Value)</Value>
        </Field>
		*/
DECLARE @pQuote uniqueidentifier; SET @pQuote='0AD7518F-F7B5-E311-A8A5-D89D6765C35C';

SELECT
   pnl_locallysourcedmachinename,
   productdescription,
   priceperunit,
   pnl_locallysourceddnp,
   pnl_locallysourcedstandarddiscount
FROM Filteredquotedetail
WHERE quoteid = @pQuote
AND pnl_islocallysourcedname = 'Yes'
ORDER BY pnl_locallysourcedmachinename DESC

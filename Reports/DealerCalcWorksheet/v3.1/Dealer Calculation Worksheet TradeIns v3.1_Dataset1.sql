DECLARE @pQuote uniqueidentifier; SET @pQuote='BE8F0D50-914B-E511-80FA-0050568A79C5';

SELECT 
	q.pnl_totalstradeinlabel as TradeinLabel,	
	IsNull(qm.pnl_machineidname, '<Unnamed Trade-in>') AS [Description],
	qm.pnl_scenario1value AS BookValue,
	qm.pnl_scenario2value as Scenario2Value,
	qm.pnl_scenario3value as Scenario3Value,
	m.pnl_config AS ConfigNumber
FROM Filteredpnl_quotemachine qm
JOIN Filteredpnl_locallysourcedmachine m ON qm.pnl_configurationid=m.pnl_locallysourcedmachineid
JOIN FilteredQuote q ON q.quoteid = qm.pnl_quoteid
WHERE qm.pnl_quoteid = @pQuote
UNION ALL
SELECT
	pnl_totalstradeinlabel as TradeinLabel,
	pnl_tradeinmachine1description AS [Description],
	pnl_tradeinmachine1bookvalue AS BookValue,
	pnl_tradeinmachine1scenario2value as Scenario2Value,
	pnl_tradeinmachine1scenario3value as Scenario3Value,
	1 AS ConfigNumber
FROM Filteredquote
WHERE quoteid = @pQuote AND pnl_tradeinmachine1description IS NOT NULL
UNION ALL
SELECT
	pnl_totalstradeinlabel as TradeinLabel,
	pnl_tradeinmachine2description AS [Description],
	pnl_tradeinmachine2bookvalue AS BookValue,
	pnl_tradeinmachine2scenario2value as Scenario2Value,
	pnl_tradeinmachine2scenario3value as Scenario3Value,
	2 AS ConfigNumber
FROM Filteredquote
WHERE quoteid = @pQuote AND pnl_tradeinmachine2description IS NOT NULL
UNION ALL
SELECT
	pnl_totalstradeinlabel as TradeinLabel,
	pnl_tradeinmachine3description AS [Description],
	pnl_tradeinmachine3bookvalue AS BookValue,
	pnl_tradeinmachine3scenario2value as Scenario2Value,
	pnl_tradeinmachine3scenario3value as Scenario3Value,
	3 AS ConfigNumber
FROM Filteredquote
WHERE quoteid = @pQuote AND pnl_tradeinmachine3description IS NOT NULL
ORDER BY 6,1,2

